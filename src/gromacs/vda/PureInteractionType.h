/*
 * PureInteractionType.h
 *
 *  Created on: Dec 13, 2020
 *      Author: Simon Christ <simonchrist@gmx.de>
 */

#ifndef SRC_GROMACS_VDA_PUREINTERACTIONTYPE_H_
#define SRC_GROMACS_VDA_PUREINTERACTIONTYPE_H_

#include <type_traits>
#include <stdexcept>
#include "InteractionType.h"

namespace vda {

/// Used for indexing detailed forces array!
/// Don't change the numbers!
enum class PureInteractionType : int
{
    BOND        = 0,
    ANGLE       = 1,
    DIHEDRAL    = 2,
    POLAR       = 3,
    COULOMB     = 4,
    LJ          = 5,
    NB14        = 6,
    NUMBER      = 7,
    POSRE       = 8
};

using T = std::underlying_type<PureInteractionType>::type;

constexpr T to_index(PureInteractionType e)
{
    return static_cast<T>(e);
}

constexpr short unsigned get_nbody(PureInteractionType e)
{
    switch(e)
    {
        case PureInteractionType::POSRE:
            return 1;
        case PureInteractionType::BOND:
        case PureInteractionType::LJ:
        case PureInteractionType::COULOMB:
        case PureInteractionType::NB14:
            return 2;
        case PureInteractionType::ANGLE:
            return 3;
        case PureInteractionType::DIHEDRAL:
            return 4;
        case PureInteractionType::NUMBER:
        case PureInteractionType::POLAR:
            return 127;  // TODO: what is NUMBER?? POLAR??
    }
    return 0;
}

/// Conversion from InteractionType into PureInteractionType
constexpr PureInteractionType to_pure(InteractionType i)
{
    switch(i) {
        case InteractionType_BOND:
            return PureInteractionType::BOND;
        case InteractionType_ANGLE:
            return PureInteractionType::ANGLE;
        case InteractionType_DIHEDRAL:
            return PureInteractionType::DIHEDRAL;
        case InteractionType_POLAR:
            return PureInteractionType::POLAR;
        case InteractionType_COULOMB:
            return PureInteractionType::COULOMB;
        case InteractionType_LJ:
            return PureInteractionType::LJ;
        case InteractionType_NB14:
            return PureInteractionType::NB14;
        case InteractionType_POSRE:
            return PureInteractionType::POSRE;
        default:
            throw std::runtime_error("Is not a pure interaction");
    }
}

/// Conversion from PureInteractionType into InteractionType
constexpr InteractionType from_pure(PureInteractionType i)
{
    switch(i) {
        case PureInteractionType::BOND:
            return InteractionType_BOND;
        case PureInteractionType::ANGLE:
            return InteractionType_ANGLE;
        case PureInteractionType::DIHEDRAL:
            return InteractionType_DIHEDRAL;
        case PureInteractionType::POLAR:
            return InteractionType_POLAR;
        case PureInteractionType::COULOMB:
            return InteractionType_COULOMB;
        case PureInteractionType::LJ:
            return InteractionType_LJ;
        case PureInteractionType::NB14:
            return InteractionType_NB14;
        default:
            throw std::runtime_error("Is not a pure interaction");
    }
}

} // namespace vda

#endif /* SRC_GROMACS_VDA_PUREINTERACTIONTYPE_H_ */
