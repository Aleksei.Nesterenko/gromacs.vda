//
// Created by christ on 14.09.20.
//

#include "gromacs/utility/filestream.h"
#include "gromacs/fileio/warninp.h"
#include "gromacs/fileio/readinp.h"
#include "gromacs/fileio/gmxfio.h"
#include "PolynomialDeformation.h"
using namespace vda;

PolynomialDeformation::PolynomialDeformation(const std::string& filename) :
    AbstractDeformation(),
    _a0(),
    _a1(),
    _a2(),
    _a3()
{
    auto *poly_file = gmx_fio_open(filename.c_str(), "r");
    
    warninp_t wi = init_warning(FALSE, 0);
    gmx::TextInputFile stream(filename);
    auto inp = read_inpfile(&stream, filename.c_str(), wi);
    // a_0
    {
        real a0_0 = get_ereal(&inp, "a0_0", 0, wi);
        real a0_1 = get_ereal(&inp, "a0_1", 0, wi);
        real a0_2 = get_ereal(&inp, "a0_2", 0, wi);
        _a0 = {a0_0, a0_1, a0_2};
    }
    // a_1
    {
        real a1_00 = get_ereal(&inp, "a1_00", 0, wi);
        real a1_01 = get_ereal(&inp, "a1_01", 0, wi);
        real a1_02 = get_ereal(&inp, "a1_02", 0, wi);
    
        real a1_10 = get_ereal(&inp, "a1_10", 0, wi);
        real a1_11 = get_ereal(&inp, "a1_11", 0, wi);
        real a1_12 = get_ereal(&inp, "a1_12", 0, wi);
    
        real a1_20 = get_ereal(&inp, "a1_20", 0, wi);
        real a1_21 = get_ereal(&inp, "a1_21", 0, wi);
        real a1_22 = get_ereal(&inp, "a1_22", 0, wi);
        _a1 = {{a1_00, a1_01, a1_02},
                {a1_10, a1_11, a1_12},
                {a1_20, a1_21, a1_22}
        };
    }
    // a_2
    {
        real a2_000 = get_ereal(&inp, "a2_000", 0, wi);
        real a2_001 = get_ereal(&inp, "a2_001", 0, wi);
        real a2_002 = get_ereal(&inp, "a2_002", 0, wi);
    
        real a2_010 = get_ereal(&inp, "a2_010", 0, wi);
        real a2_011 = get_ereal(&inp, "a2_011", 0, wi);
        real a2_012 = get_ereal(&inp, "a2_012", 0, wi);
    
        real a2_020 = get_ereal(&inp, "a2_020", 0, wi);
        real a2_021 = get_ereal(&inp, "a2_021", 0, wi);
        real a2_022 = get_ereal(&inp, "a2_022", 0, wi);
    
        real a2_100 = get_ereal(&inp, "a2_100", 0, wi);
        real a2_101 = get_ereal(&inp, "a2_101", 0, wi);
        real a2_102 = get_ereal(&inp, "a2_102", 0, wi);
    
        real a2_110 = get_ereal(&inp, "a2_110", 0, wi);
        real a2_111 = get_ereal(&inp, "a2_111", 0, wi);
        real a2_112 = get_ereal(&inp, "a2_112", 0, wi);
    
        real a2_120 = get_ereal(&inp, "a2_120", 0, wi);
        real a2_121 = get_ereal(&inp, "a2_121", 0, wi);
        real a2_122 = get_ereal(&inp, "a2_122", 0, wi);
    
        real a2_200 = get_ereal(&inp, "a2_200", 0, wi);
        real a2_201 = get_ereal(&inp, "a2_201", 0, wi);
        real a2_202 = get_ereal(&inp, "a2_202", 0, wi);
    
        real a2_210 = get_ereal(&inp, "a2_210", 0, wi);
        real a2_211 = get_ereal(&inp, "a2_211", 0, wi);
        real a2_212 = get_ereal(&inp, "a2_212", 0, wi);
    
        real a2_220 = get_ereal(&inp, "a2_220", 0, wi);
        real a2_221 = get_ereal(&inp, "a2_221", 0, wi);
        real a2_222 = get_ereal(&inp, "a2_222", 0, wi);
        _a2 = {
                {
                        {a2_000, a2_001, a2_002},
                        {a2_010, a2_011, a2_012},
                        {a2_020, a2_021, a2_022}
                },
                {
                        {a2_100, a2_101, a2_102},
                        {a2_110, a2_111, a2_112},
                        {a2_120, a2_121, a2_122}
                },
                {
                        {a2_200, a2_201, a2_202},
                        {a2_210, a2_211, a2_212},
                        {a2_220, a2_221, a2_222}
                }
        };
    }
    // a_3
    {
        real a3_0000 = get_ereal(&inp, "a3_0000", 0, wi);
        real a3_0001 = get_ereal(&inp, "a3_0001", 0, wi);
        real a3_0002 = get_ereal(&inp, "a3_0002", 0, wi);
    
        real a3_0010 = get_ereal(&inp, "a3_0010", 0, wi);
        real a3_0011 = get_ereal(&inp, "a3_0011", 0, wi);
        real a3_0012 = get_ereal(&inp, "a3_0012", 0, wi);
    
        real a3_0020 = get_ereal(&inp, "a3_0020", 0, wi);
        real a3_0021 = get_ereal(&inp, "a3_0021", 0, wi);
        real a3_0022 = get_ereal(&inp, "a3_0022", 0, wi);
    
        real a3_0100 = get_ereal(&inp, "a3_0100", 0, wi);
        real a3_0101 = get_ereal(&inp, "a3_0101", 0, wi);
        real a3_0102 = get_ereal(&inp, "a3_0102", 0, wi);
    
        real a3_0110 = get_ereal(&inp, "a3_0110", 0, wi);
        real a3_0111 = get_ereal(&inp, "a3_0111", 0, wi);
        real a3_0112 = get_ereal(&inp, "a3_0112", 0, wi);
    
        real a3_0120 = get_ereal(&inp, "a3_0120", 0, wi);
        real a3_0121 = get_ereal(&inp, "a3_0121", 0, wi);
        real a3_0122 = get_ereal(&inp, "a3_0122", 0, wi);
    
        real a3_0200 = get_ereal(&inp, "a3_0200", 0, wi);
        real a3_0201 = get_ereal(&inp, "a3_0201", 0, wi);
        real a3_0202 = get_ereal(&inp, "a3_0202", 0, wi);
    
        real a3_0210 = get_ereal(&inp, "a3_0210", 0, wi);
        real a3_0211 = get_ereal(&inp, "a3_0211", 0, wi);
        real a3_0212 = get_ereal(&inp, "a3_0212", 0, wi);
    
        real a3_0220 = get_ereal(&inp, "a3_0220", 0, wi);
        real a3_0221 = get_ereal(&inp, "a3_0221", 0, wi);
        real a3_0222 = get_ereal(&inp, "a3_0222", 0, wi);
        
        real a3_1000 = get_ereal(&inp, "a3_1000", 0, wi);
        real a3_1001 = get_ereal(&inp, "a3_1001", 0, wi);
        real a3_1002 = get_ereal(&inp, "a3_1002", 0, wi);
    
        real a3_1010 = get_ereal(&inp, "a3_1010", 0, wi);
        real a3_1011 = get_ereal(&inp, "a3_1011", 0, wi);
        real a3_1012 = get_ereal(&inp, "a3_1012", 0, wi);
    
        real a3_1020 = get_ereal(&inp, "a3_1020", 0, wi);
        real a3_1021 = get_ereal(&inp, "a3_1021", 0, wi);
        real a3_1022 = get_ereal(&inp, "a3_1022", 0, wi);
    
        real a3_1100 = get_ereal(&inp, "a3_1100", 0, wi);
        real a3_1101 = get_ereal(&inp, "a3_1101", 0, wi);
        real a3_1102 = get_ereal(&inp, "a3_1102", 0, wi);
    
        real a3_1110 = get_ereal(&inp, "a3_1110", 0, wi);
        real a3_1111 = get_ereal(&inp, "a3_1111", 0, wi);
        real a3_1112 = get_ereal(&inp, "a3_1112", 0, wi);
    
        real a3_1120 = get_ereal(&inp, "a3_1120", 0, wi);
        real a3_1121 = get_ereal(&inp, "a3_1121", 0, wi);
        real a3_1122 = get_ereal(&inp, "a3_1122", 0, wi);
    
        real a3_1200 = get_ereal(&inp, "a3_1200", 0, wi);
        real a3_1201 = get_ereal(&inp, "a3_1201", 0, wi);
        real a3_1202 = get_ereal(&inp, "a3_1202", 0, wi);
    
        real a3_1210 = get_ereal(&inp, "a3_1210", 0, wi);
        real a3_1211 = get_ereal(&inp, "a3_1211", 0, wi);
        real a3_1212 = get_ereal(&inp, "a3_1212", 0, wi);
    
        real a3_1220 = get_ereal(&inp, "a3_1220", 0, wi);
        real a3_1221 = get_ereal(&inp, "a3_1221", 0, wi);
        real a3_1222 = get_ereal(&inp, "a3_1222", 0, wi);
        
        real a3_2000 = get_ereal(&inp, "a3_2000", 0, wi);
        real a3_2001 = get_ereal(&inp, "a3_2001", 0, wi);
        real a3_2002 = get_ereal(&inp, "a3_2002", 0, wi);
        
        real a3_2010 = get_ereal(&inp, "a3_2010", 0, wi);
        real a3_2011 = get_ereal(&inp, "a3_2011", 0, wi);
        real a3_2012 = get_ereal(&inp, "a3_2012", 0, wi);
        
        real a3_2020 = get_ereal(&inp, "a3_2020", 0, wi);
        real a3_2021 = get_ereal(&inp, "a3_2021", 0, wi);
        real a3_2022 = get_ereal(&inp, "a3_2022", 0, wi);
        
        real a3_2100 = get_ereal(&inp, "a3_2100", 0, wi);
        real a3_2101 = get_ereal(&inp, "a3_2101", 0, wi);
        real a3_2102 = get_ereal(&inp, "a3_2102", 0, wi);
        
        real a3_2110 = get_ereal(&inp, "a3_2110", 0, wi);
        real a3_2111 = get_ereal(&inp, "a3_2111", 0, wi);
        real a3_2112 = get_ereal(&inp, "a3_2112", 0, wi);
        
        real a3_2120 = get_ereal(&inp, "a3_2120", 0, wi);
        real a3_2121 = get_ereal(&inp, "a3_2121", 0, wi);
        real a3_2122 = get_ereal(&inp, "a3_2122", 0, wi);
        
        real a3_2200 = get_ereal(&inp, "a3_2200", 0, wi);
        real a3_2201 = get_ereal(&inp, "a3_2201", 0, wi);
        real a3_2202 = get_ereal(&inp, "a3_2202", 0, wi);
        
        real a3_2210 = get_ereal(&inp, "a3_2210", 0, wi);
        real a3_2211 = get_ereal(&inp, "a3_2211", 0, wi);
        real a3_2212 = get_ereal(&inp, "a3_2212", 0, wi);
        
        real a3_2220 = get_ereal(&inp, "a3_2220", 0, wi);
        real a3_2221 = get_ereal(&inp, "a3_2221", 0, wi);
        real a3_2222 = get_ereal(&inp, "a3_2222", 0, wi);
        _a3 = {
                {
                        {
                                {a3_0000, a3_0001, a3_0002},
                                {a3_0010, a3_0011, a3_0012},
                                {a3_0020, a3_0021, a3_0022}
                        },
                        {
                                {a3_0100, a3_0101, a3_0102},
                                {a3_0110, a3_0111, a3_0112},
                                {a3_0120, a3_0121, a3_0122}
                        },
                        {
                                {a3_0200, a3_0201, a3_0202},
                                {a3_0210, a3_0211, a3_0212},
                                {a3_0220, a3_0221, a3_0222}
                        }
                },
        
                {
                        {
                                {a3_1000, a3_1001, a3_1002},
                                {a3_1010, a3_1011, a3_1012},
                                {a3_1020, a3_1021, a3_1022}
                        },
                        {
                                {a3_1100, a3_1101, a3_1102},
                                {a3_1110, a3_1111, a3_1112},
                                {a3_1120, a3_1121, a3_1122}
                        },
                        {
                                {a3_1200, a3_1201, a3_1202},
                                {a3_1210, a3_1211, a3_1212},
                                {a3_1220, a3_1221, a3_1222}
                        }
                },
                {
                        {
                                {a3_2000, a3_2001, a3_2002},
                                {a3_2010, a3_2011, a3_2012},
                                {a3_2020, a3_2021, a3_2022}
                        },
                        {
                                {a3_2100, a3_2101, a3_2102},
                                {a3_2110, a3_2111, a3_2112},
                                {a3_2120, a3_2121, a3_2122}
                        },
                        {
                                {a3_2200, a3_2201, a3_2202},
                                {a3_2210, a3_2211, a3_2212},
                                {a3_2220, a3_2221, a3_2222}
                        }
                }
        };
    }
    gmx_fio_close(poly_file);
}

xt::xtensor_fixed<real, xt::xshape<3>> PolynomialDeformation::derivative(rvec const& x ) const
{
    auto result =  xt::xtensor_fixed<real, xt::xshape<3>>({0, 0, 0});
    for( int i = 0; i < 3; i++ )
    {
        result(i) += _a0(i);
        
        for( int k = 0; k < 3; k++ )
        {
            result(i) += _a1(i, k) * x[k];
            
            for( int l = 0; l < 3; l++)
            {
                result(i) += _a2(i, k, l) * x[k] * x[l];
                
                for( int m = 0; m < 3; m++)
                {
                    result(i) += _a3(i, k, l, m) * x[k] * x[l] * x[m];
                }
            }
        }
    }
    return result;
}

xt::xtensor_fixed<real, xt::xshape<3, 3>> PolynomialDeformation::jacobian(rvec const& x) const
{
    auto result =  xt::xtensor_fixed<real, xt::xshape<3,3>>(xt::zeros<real>({3, 3}));
    for( int i = 0; i < 3; i++)
    {
        for( int j = 0; j < 3; j++ )
        {
            result(j, i) += _a1(j, i);
            
            for( int l = 0; l < 3; l++)
            {
                result(j, i) += _a2(j, i, l) * x[l];
                
                for( int m = 0; m < 3; m++)
                {
                    result(j, i) += _a3(j, i, l, m) * x[l] * x[m];
                }
            }
            
            for( int k = 0; k < 3; k++ )
            {
                result(j, i) += _a2(j, k, i) * x[k];
                
                for( int m = 0; m < 3; m++)
                {
                    result(j, i) += _a3(j, k, i, m) * x[k] * x[m];
                }
    
                for( int l = 0; l < 3; l++)
                {
                    result(j, i) += _a3(j, k, l, i) * x[k] * x[l];
                }
            }
        }
    }
    return result;
}
