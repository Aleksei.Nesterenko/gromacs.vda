//
// Created by christ on 18.09.20.
//

#include "UniformStretching.h"
vda::UniformStretching::UniformStretching() :
    AbstractDeformation(),
    a1_({{1./3, 0, 0},
         {0, 1./3, 0},
         {0, 0, 1./3}})
{}

xt::xtensor_fixed<real, xt::xshape<3>> vda::UniformStretching::derivative(rvec const& x) const
{
    auto result =  xt::xtensor_fixed<real, xt::xshape<3>>({0, 0, 0});
    for( int i = 0; i < 3; i++ )
    {
        for( int k = 0; k < 3; k++ )
        {
            result(i) += a1_(i, k) * x[k];
        }
    }
    return result;
}

xt::xtensor_fixed<real, xt::xshape<3, 3>> vda::UniformStretching::jacobian(rvec const& x) const
{
    auto result =  xt::xtensor_fixed<real, xt::xshape<3,3>>(xt::zeros<real>({3, 3}));
    for( int i = 0; i < 3; i++)
    {
        for( int j = 0; j < 3; j++ )
        {
            result(j, i) += a1_(j, i);
        }
    }
    return result;
}

std::string vda::UniformStretching::get_name() const
{
    return name_;
}

vda::UniformStretching::~UniformStretching() = default;
