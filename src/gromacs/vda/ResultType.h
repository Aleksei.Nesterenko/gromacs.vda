/*
 * ResultType.h
 *
 *  Created on: Dec 15, 2020
 *      Author: Simon Christ <simonchrist@gmx.de>
 */

#ifndef SRC_GROMACS_VDA_RESULTTYPE_H_
#define SRC_GROMACS_VDA_RESULTTYPE_H_

#include <cstdint>
#include <iostream>
#include <string>

namespace vda {

enum class ResultType : std::int8_t
{
    NO,                       // no storing (default)
    POINTWISE_FORCES_VECTOR,
    ENERGY_SCM
};

/// Output stream for ResultType
std::ostream& operator << (std::ostream& os, ResultType r);

/// Input stream for ResultType
std::istream& operator >> (std::istream& is, ResultType& r);

} // namespace vda

#endif /* SRC_GROMACS_VDA_RESULTTYPE_H_ */
