//
// Created by christ on 11.12.20.
//

#ifndef GROMACS_BENDINGXY_TO_YCYL_H
#define GROMACS_BENDINGXY_TO_YCYL_H

#include <xtensor/xfixed.hpp>
#include "gromacs/math/vectypes.h"
#include "gromacs/gpu_utils/hostallocator.h"
#include "AbstractDeformation.h"
namespace vda
{
class BendingXY_to_YCyl : public AbstractDeformation
{
public:
    BendingXY_to_YCyl();
    ~BendingXY_to_YCyl() override;
    
    xt::xtensor_fixed<real, xt::xshape<3>> derivative(rvec const& x) const override;
    xt::xtensor_fixed<real, xt::xshape<3, 3>> jacobian(rvec const& x) const override;

    std::string get_name() const override {return name_;}
private:
    xt::xtensor_fixed<real, xt::xshape<3, 3, 3>> a2_;
    
    std::string name_ {"bendingXY_to_Ycyl"};
};
}


#endif // GROMACS_BENDINGXY_TO_YCYL_H
