## Virtual deformation analysis (VDA)

<!-- TODO: add description about what this analysis is and what it is good for. Explain meaning of dT and dU and
 relationship to physical quantities -->
 
### Installation

### dependencies
 - [`boost`](https://www.boost.org/)
 - [`xtensor`](https://github.com/xtensor-stack/xtensor)
 

### building (commands for Unix systems)
 - clone/ download this repository and enter the directory
 - create a `build` directory e.g. `mkdir build`
 - enter that directory `cd build`
 - run cmake
 ```
    cmake -DGMX_SIMD=NONE \
          -DGMX_BINARY_SUFFIX=_vda \
          -DGMX_BUILD_MDRUN_ONLY=OFF \
          -DGMX_BUILD_VDA=ON \
          -DGMX_DEFAULT_SUFFIX=OFF \
         ..
 ```
 - run make `make -j #threads`
 - find the compiled binary in subfolder `bin`
 
### Usage

You can perform a VDA by doing a rerun
 
`gmx_vda mdrun -rerun <file> -s <tpr-file> -nt 1 -vdi <vdi-file> -vdo <output-file>`

### Additional command line flags
 - `-vdi`: VDA input file
 - `-vdo`: VDA output file


### Options

Options are specified in the VDA input file that is given to the program with `-vdi` commonly the file extension
 `.vdi` is used.

---
#### Output
VDA supports two output modi specified via `output_mode = `
 - `pointwise_forces_vector`: writes the recorded forces of all interactions for each atom
 - `energy_scm`: writes `dT` and `dU` as defined above to file.
If constraints are included it calculates the constraint forces and either treats them per atom (`dU1`) or
 homogenously distributed over the whole cluster (`dU2`) if a `constraint cluster` file was given.
From these the lower bound `dUmin` and the upper bound
 `dUmax` are calculated.
---
#### Deformations
Possible deformations can be given by `deformation = `
 - `uniform`: uniformly inflates the system
 - `stretchingXY`: stretches in the XY-plane and compresses in the Z direction to conserve volume
 - `bendingXY_to_Ycyl`: bends the XY-plane around a cylinder around a cylinder in Y direction with its center at `z
  = -∞`
 - `polynomial`: A custom deformation of the form
   `dlᵢ = a0ᵢ + a1ᵢⱼ x⃗ⱼ + a2ᵢⱼₖ x⃗ⱼ x⃗ₖ + a3ᵢⱼₖₗ x⃗ⱼ x⃗ₖ x⃗ₗ` can be
  specified by
  providing an
  additional file with
  the
  coefficients `a0`-`a3`.
  The file location should be given to the keyword `deformation_file = <path>/<file>.poly`.

For example the bending deformation could be written as
```
;; declare coefficients for vector a0, matrix a1, 3-tensor a2 and 4-tensor a3 via aX_n where n is a number with as many digits between 0 and 2 as the rank of the tensor is
;; unspecified coefficients will be treated as zero
a2_002 = 1
a2_200 = -0.5
a2_222 = -0.5
```

---
#### Domain of interest
 - The domain of interest can be a fixed box whose limits are given as `domain = xmin, xmax | ymin, ymax | zmin, zmax`
 - The domain can be set to be equal to the simulation box via `domain = box` this will adjust the box also if the
  simulation box size changes during the simulation

---
#### Constraints
If constraints are included it is necessary to give a `constraint cluster` file that gives all connected clusters
 restrained atoms, where each line is one cluster and all indices of the corresponding atoms are separated by space.

Example vdi-file:
```
; set output mode
output_mode = energy_scm
; interactions type could be one of more of:
; bond angle dihedral polar coulomb lj nb14 bonded nonbonded all
type = all
; specify deformation domain
domain = box
; specify deformation
deformation = uniform
; give cluster file for computing dU2 of constraints
constraint cluster = angle.clust
```

---
## Acknowledgements

This codebase took considerable inspiration from [`gromacs-fda`](https://github.com/HITS-MBM/gromacs-fda) by Bernd Doser
and builds upon [`GROMACS`](https://gitlab.com/gromacs/gromacs) by Mark Abraham, Berk Hess et al.

## Caveats
 - This software is not tested on windows
 - It only supports single threaded execution
 - Currently it doesn't support SIMD
 - If a fixed domain of interest is used and the simulation box changes during the simulation the fixed box should
  always fit in the simulation box
 - the analysis works the best when using velocity-verlet integration
 - it only works with coulombic electrostatics, no PME