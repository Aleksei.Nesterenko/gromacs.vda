//
// Created by christ on 13.12.20.
//

#ifndef GROMACS_VDASETTINGS_H
#define GROMACS_VDASETTINGS_H

#include <memory>
#include <fstream>
#include "gmxpre.h"
#include "gromacs/utility/real.h"
#include "gromacs/vda/boundaries/AbstractBoundary.h"
#include "gromacs/vda/deformations/AbstractDeformation.h"
#include "gromacs/commandline/filenm.h"
#include "gromacs/topology/topology.h"
#include "InteractionType.h"
#include "ResultType.h"

namespace vda
{

//! Switch regulating calculating the constraint force
enum ecforcemod
{
    ecforcemodVELOCITIES = 0,     //! calculate constraint force from velocity shift
    ecforcemodCOORDS     = 1,     //! calculate constraint force from coordinate shift
    ecforcemodNR         = 2
};
//! String corresponding to interaction modifiers
extern const char* ecforcemod_names[ecforcemodNR + 1];

class VDASettings
{
public:
    VDASettings() = default;
    ~VDASettings();
    /// Construction by input file
    VDASettings(int nfile, const t_filenm* fnm, gmx_mtop_t* mtop, matrix& box, bool parallel_execution);

    void parse_input_file(const char* pf_file_in, matrix& box);

    //TODO: think about making all these variables private

    /// Forces lower than threshold will not be considered
    real threshold_{ 1e-10 };

    /// How to compute forces
    ecforcemod constr_force_mode { ecforcemodCOORDS }; 
    
    /// Result type
    ResultType result_type_ {ResultType::NO};
    /// Filename for the results
    std::string result_filename_;
    
    /// Interaction types that are interesting, set based on input file; 
    /// functions are supposed to test against this before calculating/storing data
    /// (not implemented)
    InteractionType                           type_{ InteractionType_NONE };

    /// Should domain boundaries track the box in NPT simulations?
    /// (not implemented)
    bool                                      boundary_should_track_box{ false };
    std::shared_ptr<vda::AbstractBoundary>    boundary_;
    std::shared_ptr<vda::AbstractDeformation> deformation_;
    std::string                               cluster_filename_;
    
};

} // namespace vda
#endif // GROMACS_VDASETTINGS_H
