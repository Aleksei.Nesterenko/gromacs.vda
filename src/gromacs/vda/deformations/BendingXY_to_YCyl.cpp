//
// Created by christ on 11.12.20.
//

#include "BendingXY_to_YCyl.h"

vda::BendingXY_to_YCyl::BendingXY_to_YCyl() :
        AbstractDeformation(),
        a2_({ { { 0, 0, 1 }, { 0, 0, 0 }, { 0, 0, 0 } },
          { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } },
          { { -0.5, 0, 0 }, { 0, 0, 0 }, { 0, 0, -0.5 } } })
{}

xt::xtensor_fixed<real, xt::xshape<3>> vda::BendingXY_to_YCyl::derivative(rvec const& x) const
{
    auto result =  xt::xtensor_fixed<real, xt::xshape<3>>({0, 0, 0});
    for( int i = 0; i < 3; i++ )
    {
        for( int k = 0; k < 3; k++ )
        {
            for( int l = 0; l < 3; l++)
            {
                result(i) += a2_(i, k, l) * x[k] * x[l];
            }
        }
    }
    return result;
}

xt::xtensor_fixed<real, xt::xshape<3, 3>> vda::BendingXY_to_YCyl::jacobian(rvec const& x) const
{
    auto result =  xt::xtensor_fixed<real, xt::xshape<3,3>>(xt::zeros<real>({3, 3}));
    for( int i = 0; i < 3; i++)
    {
        for( int j = 0; j < 3; j++ )
        {
            for( int l = 0; l < 3; l++)
            {
                result(j, i) += a2_(j, i, l) * x[l];
            }
            
            for( int k = 0; k < 3; k++ )
            {
                result(j, i) += a2_(j, k, i) * x[k];
            }
        }
    }
    return result;
}

vda::BendingXY_to_YCyl::~BendingXY_to_YCyl() = default;
