//
// Created by christ on 13.12.20.
//

#include "gromacs/utility/fatalerror.h"
#include "gromacs/fileio/warninp.h"
#include "gromacs/utility/filestream.h"
#include "gromacs/fileio/readinp.h"
#include "gromacs/math/vec.h"
#include "gromacs/vda/boundaries/BoxBoundary.h"
#include "gromacs/utility.h"
#include "gromacs/vda/deformations/PolynomialDeformation.h"
#include "gromacs/vda/deformations/BendingXY_to_YCyl.h"
#include "gromacs/vda/deformations/UniformStretching.h"
#include "gromacs/utility/futil.h"
#include "gromacs/utility/cstringutil.h"
#include "gromacs/vda/deformations/StretchingXY.h"
#include "VDASettings.h"

namespace vda{

VDASettings::VDASettings(int nfile, const t_filenm* fnm, gmx_mtop_t* mtop, matrix& box, bool parallel_execution)
{
    result_filename_ = gmx_strdup(opt2fn("-vdo", nfile, fnm));
    /// Parallel execution not implemented yet
    if (parallel_execution) {
        gmx_fatal(FARGS, "VDA parallel execution not implemented yet! Please start with '-nt 1' "
                         "on the mdrun command line\n"); }
    const char *file_in = nullptr;
    if (!opt2bSet("-vdi", nfile, fnm)) {
        gmx_fatal(FARGS,
                  "No virtual deformation input file given.\n");
    }
    file_in = opt2fn("-vdi", nfile, fnm);
    parse_input_file(file_in, box);
}

void VDASettings::parse_input_file(const char* pf_file_in, matrix& box) {
    // Use GROMACS function to read lines of form key = value from input file
    warninp_t wi = init_warning(FALSE, 0);
    gmx::TextInputFile stream(pf_file_in);
    auto inp = read_inpfile(&stream, pf_file_in, wi);
    
    // Read result type
    std::stringstream(get_estr(&inp, "output_mode", "no")) >> result_type_;
    
    // Read threshold
    threshold_ = get_ereal(&inp, "threshold", 1e-10, wi);
    if (threshold_ < 0.0) {
        gmx_fatal(FARGS, "Invalid value for threshold: %f\n", threshold_); }
    std::cout << "Threshold: " << threshold_ << std::endl;
    
    std::string type_string;
    std::stringstream(get_estr(&inp, "type", "all")) >> type_string;
    type_ = from_string(type_string);
    if (type_ == InteractionType_NONE) {
        gmx_fatal(FARGS, "No interactions selected. Abort calculation.\n");
    }
    
    
    GMX_RELEASE_ASSERT(iprod(box[0], box[1]) <= sqrt(GMX_REAL_EPS), "Non-cubic boxes not yet "
                                                                    "supported.");
    real Lx = norm(box[XX]);
    real Ly = norm(box[YY]);
    real Lz = norm(box[ZZ]);
    real domain[3][2];
    size_t pos = 0;
    size_t i = 0;
    std::string domain_str = std::string(get_estr(&inp, "domain", ""));
    if( domain_str == "box" )
    {
        boundary_should_track_box = true;
        boundary_ = std::make_shared<vda::BoxBoundary> ( 0, Lx,
                                                        0, Ly,
                                                        0, Lz,
                                                        Lx, Ly, Lz
        );
    }
    else
    {
        if( domain_str.empty() )
        { 
            GMX_THROW(gmx::InvalidInputError(
                "No valid domain specified. A valid domain is e.g.:\n"
                "domain = 0, 1 | -2.3, 4 | 1.3, 5.6"));
        }
        std::string domain_bak = domain_str;
        std::string delimiter = "| ";
        do
        {
            pos = domain_str.find(delimiter);
            size_t ipos = 0;
            std::string token = domain_str.substr(0, pos);
            std::string comma = ", ";
            ipos = token.find(comma);
            real min;
            real max;
            try
            {
                min = std::stod(token.substr(0, ipos));
                max = std::stod(token.substr(ipos + comma.length(), pos));
            } catch(const std::exception &e) {
                GMX_THROW(gmx::InvalidInputError(
                    "No valid domain specified. A valid domain is e.g.:\n"
                    "domain = 0, 1 | -2.3, 4 | 1.3, 5.6"));
            }
            domain[i][0] = min;
            domain[i][1] = max;
            domain_str.erase(0, pos + delimiter.length());
            if(++i > 3) {
                gmx_fatal(FARGS, ("Too many entries for the domain limits (expected 3 "
                                  "pairs of numbers) got: " + std::to_string(i)).c_str(), domain_bak
                                  .c_str()); }
        } while ( pos != std::string::npos );
        boundary_ = std::make_shared<vda::BoxBoundary> ( domain[0][0], domain[0][1],
                                                        domain[1][0], domain[1][1],
                                                        domain[2][0], domain[2][1],
                                                        Lx, Ly, Lz
        );
    }
    // read deformation
    
    std::string deformation_str = std::string(get_estr(&inp, "deformation", ""));
    if(deformation_str == "uniform")
    {
        deformation_ = std::make_shared<vda::UniformStretching>();
    }
    else if(deformation_str == "stretchingXY")
    {
        deformation_ = std::make_shared<vda::StretchingXY>();
    }
    else if(deformation_str == "polynomial")
    {
        auto base_pos = std::string(pf_file_in).find_last_of('/') + 1;
        std::string base = std::string(pf_file_in).substr(0,base_pos);
        std::string deformation_fn(get_estr(&inp,"deformation_file", ""));
        GMX_RELEASE_ASSERT( !deformation_fn.empty(), "No file for polynomial deformation given.");
        std::string deformation_file = base + deformation_fn;
        
        deformation_ = std::make_shared<vda::PolynomialDeformation>(deformation_file);
    }
    else if(deformation_str == "bendingXY_to_Ycyl")
    {
        deformation_ = std::make_shared<vda::BendingXY_to_YCyl>();
    }
    else
    {
        GMX_THROW(gmx::InvalidInputError("No or unknown deformation specified"));
    }
    // read cluster file, if present
    std::string cluster_str = std::string(get_estr(&inp, "constraint cluster", ""));
    if( !cluster_str.empty() )
    {
        auto base_pos = std::string(pf_file_in).find_last_of('/') + 1;
        std::string base = std::string(pf_file_in).substr(0,base_pos);
        cluster_filename_ = base + cluster_str;
    }

    // read constraint force type
    constr_force_mode = (ecforcemod) get_eeenum(&inp, "constraint-force", ecforcemod_names, wi);

    check_warning_error(wi, FARGS);
}

VDASettings::~VDASettings() = default;

const char* ecforcemod_names[ecforcemodNR + 1] = {
    "from-vel", "from-coord", nullptr
};

} // namespace vda
