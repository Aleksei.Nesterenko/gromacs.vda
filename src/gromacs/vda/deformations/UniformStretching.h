//
// Created by christ on 18.09.20.
//

#ifndef GROMACS_UNIFORMSTRETCHING_H
#define GROMACS_UNIFORMSTRETCHING_H

#include <xtensor/xfixed.hpp>
#include "gromacs/math/vectypes.h"
#include "gromacs/gpu_utils/hostallocator.h"
#include "AbstractDeformation.h"
namespace vda
{
class UniformStretching : public AbstractDeformation
{
public:
    UniformStretching();
    ~UniformStretching() override;

    xt::xtensor_fixed<real, xt::xshape<3>> derivative(rvec const& x) const override;
    xt::xtensor_fixed<real, xt::xshape<3, 3>> jacobian(rvec const& x) const override;

    std::string get_name() const override;

private:
    xt::xtensor_fixed<real, xt::xshape<3, 3>> a1_;
    
    std::string name_ {"uniform_stretching"};
};
}  // namespace vda


#endif // GROMACS_UNIFORMSTRETCHING_H
