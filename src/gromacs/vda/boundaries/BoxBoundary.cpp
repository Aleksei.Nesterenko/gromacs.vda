//
// Created by christ on 14.09.20.
//

#include <cmath>
#include <vector>
#include "gromacs/math/vec.h"
#include "BoxBoundary.h"
using namespace vda;

BoxBoundary::BoxBoundary(real xmin, real xmax, real ymin, real ymax, real zmin, real zmax, 
                         real Lx, real Ly, real Lz) :
    x_bounds(xmin, xmax),
    y_bounds(ymin, ymax),
    z_bounds(zmin, zmax),
    Lx_(Lx),
    Ly_(Ly),
    Lz_(Lz)
{}

bool BoxBoundary::contains(const gmx::RVec& vec) const
{
    auto x = vec[XX];
    auto y = vec[YY];
    auto z = vec[ZZ];
    auto xmin = x_bounds.first;
    auto xmax = x_bounds.second;
    auto ymin = y_bounds.first;
    auto ymax = y_bounds.second;
    auto zmin = z_bounds.first;
    auto zmax = z_bounds.second;
    return x >= xmin && x < xmax &&
           y >= ymin && y < ymax &&
           z >= zmin && z < zmax;
}

bool BoxBoundary::contains(const gmx::RVec& vec, const gmx::RVec& rVec) const
{
    real x1 = vec[XX];
    real y1 = vec[YY];
    real z1 = vec[ZZ];
    real x2 = rVec[XX];
    real y2 = rVec[YY];
    real z2 = rVec[ZZ];
    // TODO: apply pbc here to ensure NNs
    auto xmin = x_bounds.first;
    auto xmax = x_bounds.second;
    auto ymin = y_bounds.first;
    auto ymax = y_bounds.second;
    auto zmin = z_bounds.first;
    auto zmax = z_bounds.second;
    real xlow = ceil((xmin-fmax(x1,x2)) / Lx_);
    real xhigh = floor((xmax-fmin(x1,x2)) / Lx_);
    real ylow = ceil((ymin-fmax(y1,y2)) / Ly_);
    real yhigh = floor((ymax-fmin(y1,y2)) / Ly_);
    real zlow = ceil((zmin-fmax(z1,z2)) / Lz_);
    real zhigh = floor((zmax-fmin(z1,z2)) / Lz_);
    return xlow <= xhigh && ylow <= yhigh && zlow <= zhigh;
}

std::vector<int> BoxBoundary::contains_any_image(const gmx::RVec& vec)
{
    auto x = vec[XX];
    auto y = vec[YY];
    auto z = vec[ZZ];
    auto xmin = x_bounds.first;
    auto xmax = x_bounds.second;
    auto ymin = y_bounds.first;
    auto ymax = y_bounds.second;
    auto zmin = z_bounds.first;
    auto zmax = z_bounds.second;
    int xlow = ceil((xmin-x) / Lx_);
    int xhigh = floor((xmax-x) / Lx_);
    int ylow = ceil((ymin-y) / Ly_);
    int yhigh = floor((ymax-y) / Ly_);
    int zlow = ceil((zmin-z) / Lz_);
    int zhigh = floor((zmax-z) / Lz_);
    if (xlow <= xhigh && ylow <= yhigh && zlow <= zhigh)
    {
        std::vector<int> shifts = {xlow, ylow, zlow};
        return shifts;
    }
    else
    {
        count++;
        std::vector<int> noshifts;
        return noshifts;
    }
}

void BoxBoundary::set_bounds( real xmin, real xmax, real ymin, real ymax, real zmin, real zmax )
{
    x_bounds = std::make_pair(xmin, xmax);
    y_bounds = std::make_pair(ymin, ymax);
    z_bounds = std::make_pair(zmin, zmax);
}

real BoxBoundary::get_volume() const
{
    return (x_bounds.second - x_bounds.first) * (y_bounds.second - y_bounds.first)
           * (z_bounds.second - z_bounds.first);
}
