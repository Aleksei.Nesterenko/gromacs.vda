//
// Created by christ on 14.09.20.
//

#ifndef GROMACS_BOXBOUNDARY_H
#define GROMACS_BOXBOUNDARY_H
#include <utility>
#include "gromacs/utility/real.h"
#include "AbstractBoundary.h"
namespace vda
{
class BoxBoundary : public AbstractBoundary
{
public:
    BoxBoundary(real xmin, real xmax, real ymin, real ymax, real zmin, real zmax, 
                    real Lx, real Ly, real Lz);
    ~BoxBoundary() override { std::cout<<"~BoxBoundary: Number of noshifts: " << count <<std::endl; }
    bool                  contains(const gmx::RVec& vec) const override;
    bool                  contains(const gmx::RVec& vec, const gmx::RVec& rVec) const override;
    std::vector<int>      contains_any_image(const gmx::RVec& vec) override;
    std::pair<real, real> get_x_bounds() const override { return x_bounds; }
    std::pair<real, real> get_y_bounds() const override { return y_bounds; }
    std::pair<real, real> get_z_bounds() const override { return z_bounds; }
    real                  get_volume() const override;

    void set_bounds( real xmin, real xmax, real ymin, real ymax, real zmin, real zmax ) override;
    void set_lengths( real Lx, real Ly, real Lz ) override { Lx_ = Lx; Ly_ = Ly; Lz_ = Lz; }
private:
    /// bounds of the domain of interest
    std::pair<real, real> x_bounds;
    std::pair<real, real> y_bounds;
    std::pair<real, real> z_bounds;
    /// lengths of the periodic box
    real Lx_;
    real Ly_;
    real Lz_;
};
}


#endif // GROMACS_BOXBOUNDARY_H
