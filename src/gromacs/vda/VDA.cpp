//
// Created by christ on 10.09.20.
//

#include <iomanip>
#include "gromacs/math/vec.h"
#include "gromacs/mdlib/gmx_omp_nthreads.h"
#include "gromacs/mdtypes/state.h"
#include "gromacs/pbcutil/pbc.h"
#include "gromacs/fileio/gmxfio.h"
#include "gromacs/fileio/warninp.h"
#include "gromacs/utility/filestream.h"
#include "gromacs/vda/boundaries/BoxBoundary.h"
#include "gromacs/vda/deformations/UniformStretching.h"
#include "gromacs/timing/wallcycle.h"
#include "VDA.h"

#include "config.h"
#ifdef GMX_OPENMP
#include <omp.h>
#else
int omp_get_thread_num() { return 0; }
#endif

VDA::VDA(VDA const& vda) :
        x_(vda.x_),
        xPrime_(vda.xPrime_),
        vPrime_(vda.vPrime_),
        fConstr_(vda.fConstr_),
        v_(vda.v_),
        f_(vda.f_),
        shift_vectors_(vda.shift_vectors_),
        box_(vda.box_),
        masses_(vda.masses_),
        dT_(vda.dT_),
        dU_(vda.dU_),
        dU1_(vda.dU1_),
        dU2_(vda.dU2_),
        participants_(vda.participants_),
        boundary_(vda.boundary_),
        deformation_(vda.deformation_),
        clusters_(vda.clusters_),
        vda_settings_(vda.vda_settings_),
        wcycle(vda.wcycle)
{}

VDA::VDA(int natoms, const matrix& box, vda::VDASettings& vda_settings, gmx_wallcycle_t wc) :
    x_(natoms, gmx::RVec()),
    xPrime_(natoms, gmx::RVec()),
    vPrime_(natoms, gmx::RVec()),
    fConstr_(natoms, gmx::RVec()),
    v_(natoms, gmx::RVec()),
    f_(natoms, gmx::RVec()),
    shift_vectors_(),
    box_(box),
    masses_(nullptr),
    dT_(0),
    dU_(0),
    dU1_(0),
    dU2_(0),
    participants_(),
    boundary_(vda_settings.boundary_),
    deformation_(vda_settings.deformation_),
    clusters_(),
    vda_settings_(vda_settings),
    wcycle(wc)
{
   //
   nbThreads = gmx_omp_nthreads_get(emntNonbonded);
   boThreads = gmx_omp_nthreads_get(emntBonded);
   if ( ! nbThreads ) nbThreads++;
   if ( ! boThreads ) boThreads++;

   std::cout << "Reserving " << (boThreads+nbThreads) << " arrays for interactions." << std::endl;
   for (int i=0; i< (nbThreads+boThreads); ++i)
   {
       participants_.emplace_back();
   }

   // open result file
   make_backup(vda_settings_.result_filename_);
   result_file_.open(vda_settings_.result_filename_);
   result_file_ << std::scientific << std::setprecision(6);
   result_file_ << vda_settings.result_type_ << std::endl;
   fill_constraint_clusters(); 
}

// requires c++11
VDA::VDA(const gmx::PaddedHostVector<gmx::RVec> &x,
         const gmx::PaddedHostVector<gmx::RVec> &v,
         const matrix &box, real *m, vda::VDASettings &vda_settings,
         gmx_wallcycle_t wc):
    VDA::VDA(x.size(), box, vda_settings, wc) 
{
    set_xp(&x, vdaVecCoord);
    set_xp(&x, vdaVecCoordC);
    set_xp(&v, vdaVecVel);
    set_xp(&v, vdaVecVelC);
    set_masses(m);
}

template<class T>
void VDA::set_xp(const gmx::PaddedVector<gmx::RVec, gmx::Allocator<gmx::RVec, T> > *src, 
        vdaVecChoice ch) 
{
    switch (ch) {
        case vdaVecVel:
            std::copy(src->begin(), src->end(), v_.begin() );
            break;
        case vdaVecCoord: 
            std::copy(src->begin(), src->end(), x_.begin() );
            break;
        case vdaVecCoordC:
            std::copy(src->begin(), src->end(), xPrime_.begin() );
            break;
        case vdaVecForce:
            std::copy(src->begin(), src->end(), f_.begin() );
            break;
        case vdaVecVelC:
            std::copy(src->begin(), src->end(), vPrime_.begin() );
            break;
    }
}
/// instantinating here explicitly in order not to overload functions in a header
template void VDA::set_xp(const gmx::PaddedVector<gmx::RVec, gmx::Allocator<gmx::RVec, 
                                gmx::AlignedAllocationPolicy> > *, vdaVecChoice);
template void VDA::set_xp(const gmx::PaddedVector<gmx::RVec, gmx::Allocator<gmx::RVec, 
                                gmx::HostAllocationPolicy> > *, vdaVecChoice);

void VDA::_dump_var(vdaVecChoice ch) {
    int ii = 0;
    auto array = &this->x_;
    std::string which; 
    switch (ch) {
        case vdaVecVel: 
            array = &this->v_; 
            which = "v0";
            break;
        case vdaVecCoord:
            array = &this->x_;
            which = "x0";
            break;
        case vdaVecCoordC:
            array = &this->xPrime_;
            which = "x*";
            break;
        case vdaVecVelC:
            array = &this->vPrime_;
            which = "v*";
            break;
    }
    std::cout << "DUMP: " << which << std::flush;
    for (auto &el: *array) {
        printf("| %+7.4f,%+7.4f,%+7.4f  |", el[0], el[1], el[2]);
        if (++ii > 4) break;
    }
    std::cout << std::endl;
}

void VDA::fill_constraint_clusters() 
{
    // fill constraint clusters from a file
    if( vda_settings_.cluster_filename_.empty() ) return;

    std::ifstream ifstr (vda_settings_.cluster_filename_, std::ifstream::in);

    std::istringstream iss;
    std::string line;
    while( std::getline(ifstr, line ) )
    {
        if( line[0] == ';' ) { continue; }
        iss.str(line); iss.clear();
        clusters_.emplace_back();
        for(long a; iss >> a; ) 
        {
            // -1 because atom counts are 1-based
            clusters_.back().push_back(a - 1);
        }
    }
}

VDA::~VDA()
{
    result_file_.close();
}

void VDA::save_and_write_scalar_time_averages()
{
    wallcycle_start(wcycle, ewcVDA);
    calc_dT();
    calc_dU();
    GMX_ASSERT(result_file_.is_open(), "Output file closed prematurely.");
    write_frame( result_file_ );
    nsteps_++;
    clear();
    wallcycle_stop(wcycle, ewcVDA);
}

void VDA::write_frame(std::ofstream& os) const
{
    switch (vda_settings_.result_type_)
    {
        case vda::ResultType::POINTWISE_FORCES_VECTOR:
            os << "Frame:" << nsteps_ <<"\n";
            for (const auto &pps : participants_)
            {
                for (const auto& entry : pps)
                {
                    os << entry;
                }
            }
            break;
        case vda::ResultType::ENERGY_SCM:
            if( nsteps_ == 0 )
            {
                if( deformation_->get_name() == "uniform" ) { os <<"pressure\t"; }
                os << "frame\tdT\t\tdU\t\tdU1\t\tdU2\t\tdUmin\t\tdUmax\n";
            }
            if( deformation_->get_name() == "uniform" )
            {
                os<<(dT_ + dU_) / boundary_->get_volume() <<"\t";
            }
            os <<nsteps_ <<"\t";
            os <<dT_ <<"\t";
            os <<dU_ <<"\t";
            os <<dU1_ <<"\t";
            os <<dU2_ <<"\t";
            os <<dU_ + fmin(dU1_, dU2_) <<"\t";
            os <<dU_ + fmax(dU1_, dU2_) <<"\n";
            break;
    }
    os <<std::flush;
}

void VDA::clear()
{
    dT_ = 0;
    dU_ = 0;
    dU1_ = 0;
    dU2_ = 0;
    for (auto &pp: participants_)
    {
        // it reserves capacity for the vector
        pp.clear();
    }
    shift_vectors_.clear();
}

void VDA::calc_dT() {
    // ?? paralellize with openMP ??
    // now it's not required. Profiler shows, it consumes low time.
    auto x_ptr = x_.begin();
    auto v_ptr = v_.begin();
    auto m_ptr = masses_; // just pointer
    for( const auto &shift : shift_vectors_ ) 
    {
        auto xT = *(x_ptr++); // x_[index];
        // auto shift = shift_vectors_[index];
        if(!shift.empty())
        {
            for( int i = 0; i < DIM; i++ )
            {
                xT[i] += shift[i] * box_[i][i];
            }
            auto jacobian = deformation_->jacobian(xT);
            for (int i = 0; i < DIM; i++)
            {
                for (int j = 0; j < DIM; j++)
                {
                    // these velocities are from the same particle ( no loop over participants )
                    dT_ -= (*m_ptr) * (*v_ptr)[i] * (*v_ptr)[j] * jacobian(i, j);
                }
            }
        }
        m_ptr++;
        v_ptr++;
    }
}

void VDA::_calc_dU_routine0(int pp) {
    t_pbc pbc{};
    set_pbc(&pbc, -1, box_);
    rvec dx = {0, 0, 0};
    double curSum = 0;

    for( auto& entry: participants_[pp] )
    {
        auto members = entry.get_members();
        auto forces = entry.get_forces();
        for( int i = 0; i<entry.get_num(); ++i)
        {
        // index -> members[i]
            auto xU    = x_[members[i]];
            auto shift = shift_vectors_[members[i]];
            if (shift.empty()) // check whether atom is inside the domain of interest
            {
                continue;
            }
            for (int ii = 0; ii < DIM; ii++)
            {
                xU[ii] += shift[ii] * box_[ii][ii];
            }
            for( size_t j = 0; j < entry.get_num(); j++)
            {
                auto xUp = x_[members[j]];
                pbc_dx(&pbc, xU, xUp, dx);
                // TODO: CRITICAL. virtual call must be replaced
                auto        derivative = deformation_->derivative(xU - gmx::RVec(dx));
                vda::Vector force      = forces[j];
                curSum -= entry.get_weight() * ( forces[j][XX] * derivative[XX] + forces[j][YY] * derivative[YY]
                                                + forces[j][ZZ] * derivative[ZZ] );
            }
        }
    }

#pragma omp critical
    {
        dU_ += curSum;
    }

}


void VDA::calc_dU() {
    t_pbc pbc{};
    set_pbc(&pbc, -1, box_);

    // run bonded interactions
#pragma omp parallel for num_threads(boThreads) schedule(static)
    for(int pp=0; pp<boThreads; ++pp) 
    {
        _calc_dU_routine0(pp);
    }

    // run non-bonded interactions
#pragma omp parallel for num_threads(nbThreads) schedule(static)
    for(int pp=boThreads; pp<participants_.size(); ++pp) 
    {
        _calc_dU_routine0(pp);
    }

    // check constraint cluster consistency
    for( auto& cluster: clusters_)
    {
        for(int atom_number: cluster )
        {
            GMX_RELEASE_ASSERT(fConstr_[atom_number] != gmx::RVec(), "Constraint cluster "
                                                                     "contains unconstrained atoms!");
        }
    }

    int gmx_unused nth = gmx_omp_nthreads_get(emntDefault);
    double concurentDU[nth];

 
#ifdef GMX_VDA_CALC_DU1
    // calculating dU1
    for (int i=0; i<nth; i++) concurentDU[i] = 0;

#pragma omp parallel for num_threads(nth) schedule(static)
    for (auto cl_ptr = clusters_.begin(); cl_ptr != clusters_.end(); ++cl_ptr) // go over clusters in the input file
    {
        int ctnum = omp_get_thread_num();
        for ( int index: *cl_ptr ) // go over elements in one cluster
        {
            auto xU = x_[index];
            auto shift = shift_vectors_[index];
            if( shift.empty() ) 
            {
                continue;
            }
            for (int ii = 0; ii < DIM; ii++)
            {
                xU[ii] += shift[ii] * box_[ii][ii]; // put the image into Domain of Interest
            }
            auto derivative = deformation_->derivative(xU);
            concurentDU[ctnum] -= fConstr_[index][XX] * derivative[XX] + 
                    fConstr_[index][YY] * derivative[YY] + 
                    fConstr_[index][ZZ] * derivative[ZZ];
        }
    }

    for (int i=0; i<nth; i++) dU1_ += concurentDU[i];

#endif

    // calculating dU2    
    for (int i=0; i<nth; i++) concurentDU[i] = 0;

#pragma omp parallel for num_threads(nth) schedule(static)
    for (auto cl_ptr = clusters_.begin(); cl_ptr != clusters_.end(); ++cl_ptr) // go over clusters in the input file
    {
        double subSUM = 0;
        int ctnum = omp_get_thread_num();
        rvec dx;

        for(int atom_number: *cl_ptr )
        {
            auto xU = x_[atom_number];
            auto shift = shift_vectors_[atom_number];
            if( shift.empty() )
            {
                continue;
            }
            for (int ii = 0; ii < DIM; ii++)
            {
                xU[ii] += shift[ii] * box_[ii][ii];
            }
            for( int index: *cl_ptr )
            {
                pbc_dx(&pbc, xU, x_[index], dx);
                auto derivative = deformation_->derivative(xU - gmx::RVec(dx));
                subSUM -=  fConstr_[index][XX] * derivative[XX] + 
                           fConstr_[index][YY] * derivative[YY] +
                           fConstr_[index][ZZ] * derivative[ZZ];
            }
        }
        concurentDU[ctnum] += subSUM/cl_ptr->size();
    }

    for (int i=0; i<nth; i++) dU2_ += concurentDU[i];

}

void VDA::calc_shifts()
{
    for(const auto& xi : x_)
    {
        shift_vectors_.push_back( boundary_->contains_any_image(xi));
    }
#ifndef NDEBUG
    std::cout<<"calc_shifts: Number of noshifts: " << boundary_->count <<std::endl;
#endif
}

void VDA::calc_constr_forces(real step, int integrator) 
{
    GMX_ASSERT(!EI_SD(integrator), "SD is not supported!");
    GMX_ASSERT(!EI_ENERGY_MINIMIZATION(integrator), "E-min integrators should not appear here!");

    int intFlag = 0 ? ( EI_VV(integrator) ) : 2;
    _calc_cforces(step, 
        (int) (vda_settings_.constr_force_mode) + intFlag);
}

void VDA::_calc_cforces(real step, int typ)
{
    t_pbc pbc{};
    set_pbc(&pbc, -1, box_);
    rvec dx = {0,0,0};
    int multiplier = ( (typ & 2) == 0) ? 2 : 1;
    for( auto& cluster: clusters_)
    {
        for( int i: cluster )
        {
            pbc_dx(&pbc, xPrime_[i], x_[i], dx);
            for (int j = 0; j < DIM; ++j)
            {
                if ( (typ & 1) == 0 )
                {
                    fConstr_[i][j] =
                        (vPrime_[i][j] - v_[i][j]) * 2. / step * masses_[i]
                        - f_[i][j];
                }
                else
                {
                    fConstr_[i][j] =
                        (dx[j] - v_[i][j] * step) * multiplier / (step * step) * masses_[i]
                        - f_[i][j];
                }
            }
        }
    }
}

/*
Oftenly called functions forming interaction array.
Called from corresponding core energy calculation functions.
*/
void VDA::add_posre(int i, vda::InteractionType type, rvec force)
{
    if( shift_vectors_.empty() ) { return; }

    if( shift_vectors_[i].empty() )
    {
        // this is an interaction fully outside
        return;
    }
    int ct = omp_get_thread_num();
    participants_[ct].emplace_back(1.0, vda::to_pure(type), i, force);
}

void VDA::add_nonbonded(int i, int j, vda::InteractionType type, real force, real dx, real dy, real dz)
{
    // leave early if the interaction is not interesting
    if (!(vda_settings_.type_ & type)) {return;}
    
    rvec force_v;
    force_v[0] = force * dx;
    force_v[1] = force * dy;
    force_v[2] = force * dz;
    int ct = omp_get_thread_num(); 
    add_2body_nocheck(i, j, type, force_v, boThreads+ct);
}

void VDA::add_bond(int i, int j, vda::InteractionType type, rvec force) {
    // leave early if the interaction is not interesting
    if (!(vda_settings_.type_ & type)) {return;}
    int ct = omp_get_thread_num(); 
    add_2body_nocheck(i, j, type, force, ct);
}

void VDA::add_angle(int i, int j, int k, real* f_i, real* f_j, real* f_k)
{
    if( shift_vectors_.empty() ) { return; }
    unsigned short iInside = (!shift_vectors_[i].empty() ? 1U : 0U);
    unsigned short jInside = (!shift_vectors_[j].empty() ? 2U : 0U);
    unsigned short kInside = (!shift_vectors_[k].empty() ? 4U : 0U);
    std::bitset<3> isInsides( iInside + jInside + kInside );

    real frac = static_cast<real>(isInsides.count()) / isInsides.size();

    if( frac == 0 )
    {
        return;
    }
    int ct = omp_get_thread_num();
    participants_[ct].emplace_back(1./3, vda::PureInteractionType::ANGLE,
                               i, j, k, f_i, f_j, f_k);
}

void VDA::add_dihedral(int i, int j, int k, int l, real* f_i, real* f_j, real* f_k, real* f_l)
{
    if( shift_vectors_.empty() ) { return; }
    unsigned short iInside = (!shift_vectors_[i].empty()? 1U : 0U);
    unsigned short jInside = (!shift_vectors_[j].empty()? 2U : 0U);
    unsigned short kInside = (!shift_vectors_[k].empty()? 4U : 0U);
    unsigned short lInside = (!shift_vectors_[l].empty()? 8U : 0U);
    std::bitset<4> isInsides( iInside + jInside + kInside + lInside );

    real frac = static_cast<real>(isInsides.count()) / isInsides.size();

    if( frac == 0 )
    {
        return;
    }
    int ct = omp_get_thread_num();
    participants_[ct].emplace_back(0.25, vda::PureInteractionType::DIHEDRAL,
                                i, j, k, l, f_i, f_j, f_k, f_l );
}

/// function is used only in test (pseudo-Public) 
void VDA::add_2body_nocheck(int i, int j, vda::InteractionType type, rvec force, int nthread)
{
    if( shift_vectors_.empty() ) { return; }
    bool iInside = !shift_vectors_[i].empty();
    bool jInside = !shift_vectors_[j].empty();
    if( !iInside && !jInside )
    {
        // this is an interaction fully outside
        return;
    }
    participants_[nthread].emplace_back(0.5, vda::to_pure(type), i, j, force);
}
