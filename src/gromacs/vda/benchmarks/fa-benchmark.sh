#!/bin/bash
cwd=$(pwd)
cd /Users/christ/Nextcloud/Documents/PhD/SideProjects/gromacs/fromMarkus
#grofile=400W_NVE-vv.trr
#tprfile=400W_NVE-vv.tpr
#pfifile=400W.pfi
tag=GM1
#grofile=GM1.gro
grofile=woConstraints-vv-NVE.trr
tprfile=woConstraints-vv-NVE.tpr
pfifile=woConstraints.pfi
outfile="$cwd/benchmark-$tag.txt"
gmxcmd="gmx mdrun -nt 1 -rerun $grofile -s $tprfile" 
vdacmd="/Users/christ/Projects/gromacs-vda/cmake-build-release/bin/gmx_vda mdrun -nt 1 -rerun $grofile -s $tprfile -vdi $pfifile -pfn index.ndx -vdi prod.pfi"
if [ ! -f $outfile ];
then echo "GROMACS baseline:" > $outfile
    echo $gmxcmd >> $outfile
    gtime -f "Elapsed:\t%es\nMemory :\t%MkB" -a -o $outfile $gmxcmd;
fi
echo "" >> $outfile
echo "GMX-VDA:" >> $outfile
echo $(date) >> $outfile
gtime -f "Elapsed:\t%es\nMemory :\t%MkB" -a -o $outfile $vdacmd
cd -
