//
// Created by christ on 17.12.20.
//

#ifndef GROMACS_STRETCHINGXY_H
#define GROMACS_STRETCHINGXY_H

#include <xtensor/xfixed.hpp>
#include "gromacs/math/vectypes.h"
#include "gromacs/gpu_utils/hostallocator.h"
#include "AbstractDeformation.h"
namespace vda
{
class StretchingXY : public AbstractDeformation
{
public:
    StretchingXY();
    ~StretchingXY() override;
    
    xt::xtensor_fixed<real, xt::xshape<3>> derivative(rvec const& x) const override;
    xt::xtensor_fixed<real, xt::xshape<3, 3>> jacobian(rvec const& x) const override;

    std::string get_name() const override;

private:
    xt::xtensor_fixed<real, xt::xshape<3, 3>> a1_;
    
    std::string name_ {"stretching_XY"};
};
}  // namespace vda


#endif // GROMACS_STRETCHINGXY_H
