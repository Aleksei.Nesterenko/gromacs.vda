/*
 * ResultType.cpp
 *
 *  Created on: Dec 15, 2020
 *      Author: Simon Christ <simonchrist@gmx.de>
 */

#include <algorithm>
#include <cctype>
#include <stdexcept>
#include "ResultType.h"

namespace vda {

std::ostream& operator << (std::ostream& os, ResultType r)
{
    switch(r) {
        case ResultType::NO:
            return os << "no";
        case ResultType::POINTWISE_FORCES_VECTOR:
            return os << "pointwise_forces_vector";
        case ResultType::ENERGY_SCM:
            return os << "Schullian-Christ-Miettinen Energy";
        default:
            return os << "invalid";
    }
}

std::istream& operator >> (std::istream& is, ResultType& r)
{
    std::string s;
    is >> s;
    std::transform(s.begin(), s.end(), s.begin(), tolower);
    if (s == "no")
        r = ResultType::NO;
    else if (s == "pointwise_forces_vector")
        r = ResultType::POINTWISE_FORCES_VECTOR;
    else if (s == "energy_scm")
        r = ResultType::ENERGY_SCM;
    else
        throw std::runtime_error("Unknown option " + s);
    return is;
}

} // namespace vda
