//
// Created by christ on 19.11.20.
//

#include <utility>
#include "ParticipatingInteraction.h"

vda::ParticipatingInteraction::ParticipatingInteraction(real weight,
                                                       vda::PureInteractionType et,
                                                       vda::Vector *forces,
                                                       int *members) :
    interactionType(et), weight_(weight)
{
    for (int i=0; i<get_nbody(et); ++i)
    {
        members_[i] = members[i];
        forces_[i] = forces[i];
    }   
}

vda::ParticipatingInteraction::ParticipatingInteraction(real weight, vda::PureInteractionType et,
                             int i, const vda::Vector &fi) :
    interactionType(et), weight_(weight)
{
    members_[0] = i;
    forces_[0] += fi;
}


vda::ParticipatingInteraction::ParticipatingInteraction(real weight, vda::PureInteractionType et,
                             int i, int j, const vda::Vector &fi) :
    interactionType(et), weight_(weight)
{
    members_[0] = i;
    members_[1] = j;
    forces_[0] += fi;
    for (int ii=0;ii<DIM;++ii)
    {
        forces_[1][ii] = -forces_[0][ii];
    }
}

vda::ParticipatingInteraction::ParticipatingInteraction(real weight, vda::PureInteractionType et,
                             int i, int j, int k, const vda::Vector &fi, const vda::Vector &fj,
                             const vda::Vector &fk) :
    interactionType(et), weight_(weight)
{
    members_[0] = i;
    members_[1] = j;
    members_[2] = k;
    forces_[0] += fi;
    forces_[1] += fj;
    forces_[2] += fk;
}

vda::ParticipatingInteraction::ParticipatingInteraction(real weight, vda::PureInteractionType et,
                             int i, int j, int k, int l, const vda::Vector &fi, const vda::Vector &fj,
                             const vda::Vector &fk, const vda::Vector &fl) :
    interactionType(et), weight_(weight)
{
    members_[0] = i;
    members_[1] = j;
    members_[2] = k;
    members_[3] = l;
    forces_[0] += fi;
    forces_[1] += fj;
    forces_[2] += fk;
    forces_[3] += fl;
}

vda::ParticipatingInteraction::ParticipatingInteraction(ParticipatingInteraction const& pi) :
    interactionType(pi.interactionType), weight_(pi.weight_)
{
    for (int i=0; i<get_nbody(interactionType); ++i)
    {
        members_[i] = pi.members_[i];
        forces_[i] = pi.forces_[i];
    }
}
