//
// Created by christ on 11.09.20.
//

#ifndef GROMACS_ABSTRACTDEFORMATION_H
#define GROMACS_ABSTRACTDEFORMATION_H

#include <xtensor/xshape.hpp>
namespace vda
{
class AbstractDeformation
{
public:
    AbstractDeformation() = default;
    virtual ~AbstractDeformation() = default;
    
    virtual xt::xtensor_fixed<real, xt::xshape<3>> derivative(rvec const& x) const = 0;
    virtual xt::xtensor_fixed<real, xt::xshape<3,3>> jacobian(rvec const& x) const = 0;
    
    virtual std::string get_name() const = 0;
};
}


#endif // GROMACS_ABSTRACTDEFORMATION_H
