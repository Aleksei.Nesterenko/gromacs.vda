//
// Created by christ on 16.09.20.
//

#include <gtest/gtest.h>
#include <utility>

#include "gromacs/fileio/tpxio.h"
#include "gromacs/mdtypes/inputrec.h"
#include "gromacs/fileio/groio.h"
#include "gromacs/mdtypes/state.h"
#include "gromacs/timing/wallcycle.h"
#include "gromacs/gmxlib/network.h"
#include "gromacs/utility/gmxmpi.h"

#include "testutils/stringtest.h"
#include "testutils/cmdlinetest.h"
#include "testutils/testfilemanager.h"

#include "gromacs/math/vec.h"
#include "gromacs/vda/VDA.h"
#include "gromacs/vda/deformations/UniformStretching.h"



namespace vda
{
class VDATest : public gmx::test::CommandLineTestBase
//    public gmx::test::StringTestBase,
//    public ::testing::WithParamIntervdace<GromacsFileType>
{
public:
    VDATest() :
        CommandLineTestBase(),
        frameData()
    {
        prefix_ = (std::string(gmx::test::TestFileManager::getInputDataDirectory()) + 
                    "/data/"); // (not ideal) repeating init of static var
        assert( testGroFiles_.size() == testTprFiles_.size() );
        for(int i = 0; i < static_cast<int>(testGroFiles_.size()); i++)
        {
            auto grofilename(testGroFiles_[i]);
            auto tprfilename(testTprFiles_[i]);
            auto vdifilename(testPfiFiles_[i]);
            frameData.emplace(grofilename,create_reference(grofilename, tprfilename, vdifilename));
        }

        auto communicator = MPI_COMM_WORLD;
        CommrecHandle crHandle = init_commrec(communicator, nullptr);
        t_commrec*    cr       = crHandle.get();
        wcycle = wallcycle_init(stdout, -1, cr);
    }

protected:
    gmx_wallcycle_t wcycle;
    class FrameData
    {
    public:
        FrameData() :
            refX_(),
            refV_(),
            refBox_()
        {}

        FrameData(const std::string& grofile,
                  const std::string& vdifile,
                  int nAtoms) :
                refX_(nAtoms),
                refV_(nAtoms)
        {
            real x[nAtoms][DIM];
            real v[nAtoms][DIM];
            t_atoms     atoms; 
            t_symtab    symtab;
            matrix      box;
            open_symtab(&symtab);
            init_t_atoms(&atoms, nAtoms, false);
            gmx_gro_read_conf(grofile.c_str(), &symtab, nullptr, &atoms, x,
                              v, box);

            // TODO: handle forces - DO WE REALLY NEED IT?
            for( int i = 0; i < nAtoms; i++)
            {
                refX_[i] = x[i];
                refV_[i] = v[i];
            }
            for (int i = 0; i < DIM; ++i)
            {
                for (int j = 0; j < DIM; ++j)
                {
                    refBox_[i][j] = box[i][j];
                }
            }
            vda_settings_.parse_input_file(vdifile.c_str(), box);
            vda_settings_.result_filename_ = prefix_ + "test_output.vdi";
        }

        FrameData(const std::string& tprfile,
                  const std::string& vdifile)
        {
            auto header = readTpxHeader( tprfile.c_str(), false );
            int nAtoms = header.natoms;
            t_state state;
            gmx_mtop_t top;
            t_inputrec inputrec;
            FILE* fplog = nullptr;
            auto pDesFile = read_tpx_state( tprfile.c_str(), &inputrec, &state, &top);
            auto mdAtoms = gmx::makeMDAtoms(fplog, top, inputrec, false);
            // TODO: handle forces - DO WE REALLY NEED IT?
            atoms2md(&top, &inputrec, -1, nullptr, nAtoms, mdAtoms.get());
            for( int i = 0; i < nAtoms; i++ )
            {
                masses_.push_back(mdAtoms->mdatoms()->massT[i]);
            }
            refX_.resizeWithPadding(nAtoms);
            refV_.resizeWithPadding(nAtoms);
            for( int i = 0; i < nAtoms; i++)
            {
               refX_[i] = state.x[i];
               refV_[i] = state.v[i];
            }
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    refBox_[i][j] = state.box[i][j];
                }
            }
            vda_settings_.parse_input_file(vdifile.c_str(), state.box);
            vda_settings_.result_filename_ = prefix_ + "test_output.vdi";
        }
        
        FrameData(gmx::PaddedHostVector<gmx::RVec> x,
                  gmx::PaddedHostVector<gmx::RVec> v, 
                  matrix box) :
            refX_(std::move(x)),
            refV_(std::move(v)),
            refBox_{{box[0][0], box[0][1], box[0][2]},
                    {box[1][0], box[1][1], box[1][2]},
                    {box[2][0], box[2][1], box[2][2]}}
        {}
        
        FrameData( const FrameData& fd ) :
                refBox_(),
                vda_settings_(fd.vda_settings_)
        {
            refX_ = fd.refX_;
            refV_ = fd.refV_;
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    refBox_[i][j] = fd.refBox_[i][j];
                }
            }
            masses_ = fd.masses_;
        }

        FrameData& operator=(const FrameData& fd)
        {
            refX_ = fd.refX_;
            refV_ = fd.refV_;
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    refBox_[i][j] = fd.refBox_[i][j];
                }
            }
            masses_ = fd.masses_;
            vda_settings_ = fd.vda_settings_;
            return *this;
        }
        
        void read_masses( const std::string& tprfile)
        {
            t_state state;
            gmx_mtop_t top;
            t_inputrec inputrec;
            FILE* fplog = nullptr;
            auto pDesFile = read_tpx_state( tprfile.c_str(), &inputrec, &state, &top);
            auto mdAtoms = gmx::makeMDAtoms(fplog, top, inputrec, false);
            atoms2md(&top, &inputrec, -1, nullptr, top.natoms, mdAtoms.get());
            for( int i = 0; i < top.natoms; i++ )
            {
                masses_.push_back(mdAtoms->mdatoms()->massT[i]);
            }
        }

        gmx::PaddedHostVector<gmx::RVec> refX_;
        gmx::PaddedHostVector<gmx::RVec> refV_;
        std::vector<real>                masses_ {};
        matrix                           refBox_;
        vda::VDASettings                 vda_settings_;
    }; // end FrameData class definition

    std::map<std::string, FrameData> frameData;
    
    static std::string prefix_;
    
    FrameData create_reference(std::string grofilename, 
                               std::string tprfilename, 
                               std::string vdifilename)
    {
        auto tprfile = prefix_ + tprfilename;
        auto header = readTpxHeader( tprfile.c_str(), false );
        int nAtoms = header.natoms;
        FrameData data(prefix_ + grofilename, prefix_ + vdifilename
                , nAtoms);
        data.read_masses(prefix_ + tprfilename);
        return data;
    }

    std::vector<std::string> testGroFiles_{
        "init.gro",           "twoAtBoundary.gro",   "threeInit.gro",
        "three2.gro",         "threeOne.gro",        "threeOneXY.gro",
        "threePoly.gro",      "octaneWaterInit.gro", "octaneWaterPoly.gro",
        "decaneWaterInit.gro"
    };
    std::vector<std::string> testTprFiles_{ 
        "two.tpr",        "two.tpr",         "three.tpr",
        "three.tpr",      "three.tpr",       "three.tpr",
        "three.tpr",      "octaneWater.tpr", "octaneWater.tpr",
        "decaneWater.tpr" };
    std::vector<std::string> testPfiFiles_{ 
        "default_box.vdi", "default_box.vdi", "default_box.vdi",
        "default_box.vdi", "one_box.vdi",     "one_xy_box.vdi",
        "one_xy_poly.vdi", "default_box.vdi", "one_xy_poly.vdi",
        "default_box.vdi" };
};

std::string VDATest::prefix_;

TEST_F(VDATest, ReadGroFile)
{
    FrameData frame(prefix_ + "init.gro", prefix_ +"prod.vdi", 2);
    auto x = frame.refX_;
    auto v = frame.refV_;
    PaddedVector<gmx::RVec> refX_({ gmx::RVec(1.605, 2.058, 1.712), 
                                    gmx::RVec(2.083, 1.983, 1.652) });
    PaddedVector<gmx::RVec> refV_({ gmx::RVec(0.1814, 0.5446, 0.0734), 
                                    gmx::RVec(0.2052, 0.1291, 0.0301) });
    EXPECT_EQ(x[0][0], refX_[0][0]);
    EXPECT_EQ(x[0][1], refX_[0][1]);
    EXPECT_EQ(x[0][2], refX_[0][2]);
    EXPECT_EQ(x[1][0], refX_[1][0]);
    EXPECT_EQ(x[1][1], refX_[1][1]);
    EXPECT_EQ(x[1][2], refX_[1][2]);
    EXPECT_EQ(v[0][0], refV_[0][0]);
    EXPECT_EQ(v[0][1], refV_[0][1]);
    EXPECT_EQ(v[0][2], refV_[0][2]);
    EXPECT_EQ(v[1][0], refV_[1][0]);
    EXPECT_EQ(v[1][1], refV_[1][1]);
    EXPECT_EQ(v[1][2], refV_[1][2]);
}

TEST_F(VDATest, DeformationGradient)
{
    auto refX_ = frameData.at("init.gro").refX_;
    vda::UniformStretching uniformStretching;
    auto gradient0 = uniformStretching.derivative(refX_[0]);
    auto gradient1 = uniformStretching.derivative(refX_[1]);
    EXPECT_FLOAT_EQ(gradient1(0), 0.69433331);
    EXPECT_FLOAT_EQ(gradient1(1), 0.6610);
    EXPECT_FLOAT_EQ(gradient1(2), 0.55066669);
    EXPECT_FLOAT_EQ(gradient0(0), 0.535);
    EXPECT_FLOAT_EQ(gradient0(1), 0.686);
    EXPECT_FLOAT_EQ(gradient0(2), 0.5706667);
}

TEST_F(VDATest, Virial_2Coulomb)
{
    rvec ref_LJ = {117.161, -18.383, -14.7064};
    rvec ref_Coulomb = {-34.8752, 5.47206, 4.37765};
    auto frame = create_reference("charged_init.gro", "chargedWater.tpr", "default_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(1, 0, vda::InteractionType_LJ, ref_LJ, 1);
    vda_instance.add_2body_nocheck(1, 0, vda::InteractionType_COULOMB, ref_Coulomb, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -9.469501);
    EXPECT_NEAR(vda_instance.get_dU(), -13.64024, 1e-4);
}

TEST_F(VDATest, Virial_2LJ)
{
    // !! ATM this checks should work if one particle is outside of the boundary !!
    // Virial is dU, that can be calculated by (gmx -rerun ->)
    //  gmx energy -f ener.edr -b 0 -e 0
    //  (Average of Diagonal): -9.710652 for the first frame
    // !! they calculate half of our virial !!

    // Box volume: 48.3989
    rvec refForce_{ 117.161, -18.383, -14.7064 };
    auto frame = frameData.at("init.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(1, 0, vda::InteractionType_LJ, refForce_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -9.4695015);
    EXPECT_NEAR(vda_instance.get_dU(), 2 * -9.71067, 1e-4);
    EXPECT_NEAR(vda_instance.get_dU(), 2 * -9.710652, 1e-4);
}

TEST_F(VDATest, Virial_2LJ_Edge)
{
    // !! they calculate half of our virial !!
    rvec refForce_{ -7.19098, -1.97778, -2.55149 };
    auto frame = frameData.at("twoAtBoundary.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(1, 0, vda::InteractionType_LJ, refForce_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    EXPECT_NEAR(vda_instance.get_dT(), -3.27047, 1e-4);
    EXPECT_NEAR(vda_instance.get_dU(), -1.37178, 1e-4 );
}

TEST_F(VDATest, Virial_3LJ)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ -1542.81, -1277.74, -343.61 };
    rvec refForce21_{ 174.062, -201.783, -138.605 };
    rvec refForce02_{ 23.6815, -2.13592, -5.87681 };
    auto frame = frameData.at("threeInit.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(2, 1, vda::InteractionType_LJ, refForce21_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -3.808044);
    EXPECT_NEAR(vda_instance.get_dU(), -322.23314, 1e-2);
}

TEST_F(VDATest, Virial_3LJ_OneXY)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ 6.14138, 4.84028, 1.46898 };
    rvec refForce21_{ -6.3505, 19.8796, 12.6581 };
    rvec refForce02_{ 8.36627, -0.0228192, -1.76841 };
    auto frame = frameData.at("threeOneXY.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(2, 1, vda::InteractionType_LJ, refForce21_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -18.82078);
    EXPECT_NEAR(vda_instance.get_dU(), 5.45592, 1e-2);
}

TEST_F(VDATest, Virial_3LJ_One)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ 6.14138, 4.84028, 1.46898 };
    rvec refForce21_{ -6.3505, 19.8796, 12.6581 };
    rvec refForce02_{ 8.36627, -0.0228192, -1.76841 };
    auto frame = frameData.at("threeOne.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(2, 1, vda::InteractionType_LJ, refForce21_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), 0);
    EXPECT_NEAR(vda_instance.get_dU(), 0, 1e-2);
}

TEST_F(VDATest, Virial_3LJ_2_POLY)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ 6.14138, 4.84028, 1.46898 };
    rvec refForce21_{ -6.3505, 19.8796, 12.6581 };
    rvec refForce02_{ 8.36627, -0.0228192, -1.76841 };
    auto frame = frameData.at("threePoly.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(2, 1, vda::InteractionType_LJ, refForce21_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_NEAR(vda_instance.get_dT(), 4.666893, 1e-5);
//    EXPECT_NEAR(vda_instance.get_dU(), 8.7754, 1e-2);
    EXPECT_FLOAT_EQ(vda_instance.get_dU(), 13.009437);
}

TEST_F(VDATest, Virial_3LJ_2)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ 6.14138, 4.84028, 1.46898 };
    rvec refForce21_{ -6.3505, 19.8796, 12.6581 };
    rvec refForce02_{ 8.36627, -0.0228192, -1.76841 };
    auto frame = frameData.at("three2.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(2, 1, vda::InteractionType_LJ, refForce21_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -40.958054);
    EXPECT_NEAR(vda_instance.get_dU(), 8.7754, 1e-2);
}

TEST_F(VDATest, Bending_OctaneWater)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ -617.129, -511.1, -137.445 };
    rvec refForce12_{ -2.9456, 3.41471, 2.34557 };
    rvec refForce02_{ 9.47272, -0.854378, -2.35075 };
    auto frame = create_reference("octaneWaterInit.gro", "octaneWater.tpr", "bending_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refForce12_, 0);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    auto framePoly = create_reference("octaneWaterInit.gro", "octaneWater.tpr", "bending_poly_box"
                                      ".vdi");
    auto refX_Poly = framePoly.refX_;
    auto refV_Poly = framePoly.refV_;
    VDA   vda_instancePoly(refX_Poly, refV_Poly, framePoly.refBox_, framePoly.masses_.data(), 
        framePoly.vda_settings_, wcycle);
    vda_instancePoly.calc_shifts();
    vda_instancePoly.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instancePoly.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refForce12_, 0);
    vda_instancePoly.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instancePoly.calc_dT();
    vda_instancePoly.calc_dU();
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), vda_instancePoly.get_dT());
    EXPECT_FLOAT_EQ(vda_instance.get_dU(), vda_instancePoly.get_dU());
}

TEST_F(VDATest, Virial_OctaneWater)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ -617.129, -511.1, -137.445 };
    rvec refForce12_{ -2.9456, 3.41471, 2.34557 };
    rvec refForce02_{ 9.47272, -0.854378, -2.35075 };
    auto frame = frameData.at("octaneWaterInit.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refForce12_, 0);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -3.808044);
    EXPECT_NEAR(vda_instance.get_dU(), -111.023, 1e-2);
}

TEST_F(VDATest, Poly_OctaneWater)
{
    // !! they calculate half of our virial !!
    rvec refForce01_{ -617.129, -511.1, -137.445 };
    rvec refForce12_{ -2.9456, 3.41471, 2.34557 };
    rvec refForce02_{ 9.47272, -0.854378, -2.35075 };
    auto frame = frameData.at("octaneWaterPoly.gro");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refForce12_, 0);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -7.1532278);
    EXPECT_NEAR(vda_instance.get_dU(), -77.70, 1e-2);
}

TEST_F(VDATest, Virial_DecaneWater)
{
    // !! they calculate half of our virial !!
    // Bonds
    rvec refBond12_{ 45.1725, -302.036, -103.188 };
    rvec refBond23_{ -276.797, -525.621, -596.292 };
    // Angles
    rvec refAngle123_[3]{{3.50846, -0.748477, 3.72672},
                         {-5.51568, 3.09761, -4.86568},
                         {2.00722, -2.34913, 1.13897}
                        };
    // TODO: velocities are wrong when run from the command line (also .gro and .trr give
    //  different results) !!
    auto frame = create_reference("decaneWater.gro", "decaneWater.tpr", "default_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refBond12_, 0);
    vda_instance.add_2body_nocheck(2, 3, vda::InteractionType_BOND, refBond23_, 0);
    vda_instance.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -1963.2623);
    EXPECT_NEAR(vda_instance.get_dU(), 399.09, 1e-1);
}

TEST_F(VDATest, Virial_DecanePoly_PureAngle)
{
    // !! they calculate half of our virial !!
    // Angles
    rvec refAngle123_[3]{{3.50846, -0.748477, 3.72672},
            {-5.51568, 3.09761, -4.86568},
            {2.00722, -2.34913, 1.13897}
    };
    auto frame = create_reference("decaneWater.gro", "decaneWater.tpr", "decanePoly.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_NEAR(vda_instance.get_dT(), 1230.21, 1e-2);
    EXPECT_NEAR(vda_instance.get_dU(), -5.24078, 1e-4);
}

TEST_F(VDATest, Poly_DecaneWater)
{
    // !! they calculate half of our virial !!
    // Bonds
    rvec refBond12_{ 45.1725, -302.036, -103.188 };
    rvec refBond23_{ -276.797, -525.621, -596.292 };
    // Angles
    rvec refAngle123_[3]{{3.50846, -0.748477, 3.72672},
            {-5.51568, 3.09761, -4.86568},
            {2.00722, -2.34913, 1.13897}
    };
    auto frame = create_reference("decaneWater.gro", "decaneWater.tpr", "decanePoly.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refBond12_, 0);
    vda_instance.add_2body_nocheck(2, 3, vda::InteractionType_BOND, refBond23_, 0);
    vda_instance.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_NEAR(vda_instance.get_dT(), 1230.21, 1e-2);
    EXPECT_NEAR(vda_instance.get_dU(), 2690.52, 1e-2);
}

TEST_F(VDATest, Bending_Dihedral_Half)
{
    // !! they calculate half of our virial !!
    // LJ
    rvec refForce01_{ -0.263863, 0.00894439, 1.82692 };
    rvec refForce02_{ 0.887961, -0.525766, 2.08554 };
    rvec refForce03_{ 0.722422, -1.03668, 0.868713 };
    rvec refForce04_{ 0.498928, -0.930625, 0.073129 };
    rvec refForce42_{ -0.239987, 1.22817, 1.31489 };
    rvec refForce31_{ -1.68605, 1.88135, 1.09365 };
    // Bonds
    rvec refBond12_{ 1.9578, -0.853636, -0.477851 };
    rvec refBond23_{ -0.590743, 2.42451, 1.43378 };
    rvec refBond34_{ 0.0916361, -0.856599, -1.66937 };
    // Angles
    rvec refAngle123_[3]{ { 7.03468, 12.0566, 7.28362 },
                          { -22.5764, -14.7529, -9.12773 },
                          { 15.5417, 2.69626, 1.84411 } };
    rvec refAngle234_[3]{ { 1.07256, -2.1714, 4.11372 },
                          { -2.49495, 6.15754, -6.23719 },
                          { 1.42239, -3.98614, 2.12348 } };
    // Dihedrals
    rvec refDihedral1234_[4]{ { -2.6624, -2.38199, -6.65288 },
                              { 0.835379, 5.83056, 0.068634 },
                              { 11.4507, -11.5021, 11.245 },
                              { -9.6237, 8.05354, -4.66076 } };
    auto frame = create_reference("withDihedral_closeBy.gro", "closeBy.tpr",
                                  "bending_xhalf_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.add_2body_nocheck(0, 3, vda::InteractionType_LJ, refForce03_, 1);
    vda_instance.add_2body_nocheck(0, 4, vda::InteractionType_LJ, refForce04_, 1);
    vda_instance.add_2body_nocheck(3, 1, vda::InteractionType_LJ, refForce31_, 1);
    vda_instance.add_2body_nocheck(4, 2, vda::InteractionType_LJ, refForce42_, 1);
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refBond12_, 0);
    vda_instance.add_2body_nocheck(2, 3, vda::InteractionType_BOND, refBond23_, 0);
    vda_instance.add_2body_nocheck(3, 4, vda::InteractionType_BOND, refBond34_, 0);
    vda_instance.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance.add_angle(2, 3, 4, refAngle234_[0], refAngle234_[1], refAngle234_[2]);
    vda_instance.add_dihedral(1, 2, 3, 4, refDihedral1234_[0], refDihedral1234_[1],
                             refDihedral1234_[2], refDihedral1234_[3]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    EXPECT_NEAR(vda_instance.get_dT(), -3345.882, 1e-3);
    EXPECT_NEAR(vda_instance.get_dU(), -2.61199, 1e-4);
}

TEST_F(VDATest, Bending_Dihedral)
{
    // !! they calculate half of our virial !!
    //LJ
    rvec refForce01_{-0.263863, 0.00894439, 1.82692};
    rvec refForce02_{0.887961, -0.525766, 2.08554};
    rvec refForce03_{0.722422, -1.03668, 0.868713};
    rvec refForce04_{0.498928, -0.930625, 0.073129};
    rvec refForce42_{-0.239987, 1.22817, 1.31489};
    rvec refForce31_{-1.68605, 1.88135, 1.09365};
    // Bonds
    rvec refBond12_{ 1.9578, -0.853636, -0.477851 };
    rvec refBond23_{ -0.590743, 2.42451, 1.43378 };
    rvec refBond34_{ 0.0916361, -0.856599, -1.66937 };
    // Angles
    rvec refAngle123_[3]{{7.03468, 12.0566, 7.28362},
            {-22.5764, -14.7529, -9.12773},
            {15.5417, 2.69626, 1.84411}
    };
    rvec refAngle234_[3]{{1.07256, -2.1714, 4.11372},
            {-2.49495, 6.15754, -6.23719},
            {1.42239, -3.98614, 2.12348}
    };
    //Dihedrals
    rvec refDihedral1234_[4]{
            {-2.6624, -2.38199, -6.65288},
            {0.835379, 5.83056, 0.068634},
            {11.4507, -11.5021, 11.245},
            {-9.6237, 8.05354, -4.66076}
    };
    auto frame = create_reference("withDihedral_closeBy.gro", "closeBy.tpr", "bending_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.add_2body_nocheck(0, 3, vda::InteractionType_LJ, refForce03_, 1);
    vda_instance.add_2body_nocheck(0, 4, vda::InteractionType_LJ, refForce04_, 1);
    vda_instance.add_2body_nocheck(3, 1, vda::InteractionType_LJ, refForce31_, 1);
    vda_instance.add_2body_nocheck(4, 2, vda::InteractionType_LJ, refForce42_, 1);
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refBond12_, 0);
    vda_instance.add_2body_nocheck(2, 3, vda::InteractionType_BOND, refBond23_, 0);
    vda_instance.add_2body_nocheck(3, 4, vda::InteractionType_BOND, refBond34_, 0);
    vda_instance.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance.add_angle(2, 3, 4, refAngle234_[0], refAngle234_[1], refAngle234_[2]);
    vda_instance.add_dihedral(1, 2, 3, 4, refDihedral1234_[0], refDihedral1234_[1],
                             refDihedral1234_[2], refDihedral1234_[3]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    auto framePoly = create_reference("withDihedral_closeBy.gro", "closeBy.tpr", 
        "bending_poly_box.vdi");
    auto refX_Poly = framePoly.refX_;
    auto refV_Poly = framePoly.refV_;
    VDA   vda_instance_Poly(refX_Poly, refV_Poly, framePoly.refBox_, framePoly.masses_.data(), 
    framePoly.vda_settings_, wcycle);
    vda_instance_Poly.calc_shifts();
    vda_instance_Poly.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance_Poly.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance_Poly.add_2body_nocheck(0, 3, vda::InteractionType_LJ, refForce03_, 1);
    vda_instance_Poly.add_2body_nocheck(0, 4, vda::InteractionType_LJ, refForce04_, 1);
    vda_instance_Poly.add_2body_nocheck(3, 1, vda::InteractionType_LJ, refForce31_, 1);
    vda_instance_Poly.add_2body_nocheck(4, 2, vda::InteractionType_LJ, refForce42_, 1);
    vda_instance_Poly.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refBond12_, 0);
    vda_instance_Poly.add_2body_nocheck(2, 3, vda::InteractionType_BOND, refBond23_, 0);
    vda_instance_Poly.add_2body_nocheck(3, 4, vda::InteractionType_BOND, refBond34_, 0);
    vda_instance_Poly.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance_Poly.add_angle(2, 3, 4, refAngle234_[0], refAngle234_[1], refAngle234_[2]);
    vda_instance_Poly.add_dihedral(1, 2, 3, 4, refDihedral1234_[0], refDihedral1234_[1],
                             refDihedral1234_[2], refDihedral1234_[3]);
    vda_instance_Poly.calc_dT();
    vda_instance_Poly.calc_dU();
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), vda_instance_Poly.get_dT());
    EXPECT_FLOAT_EQ(vda_instance.get_dU(), vda_instance_Poly.get_dU());
}

TEST_F(VDATest, Poly_Dihedral)
{
    // !! they calculate half of our virial !!
    //LJ
    rvec refForce01_{-0.263863, 0.00894439, 1.82692};
    rvec refForce02_{0.887961, -0.525766, 2.08554};
    rvec refForce03_{0.722422, -1.03668, 0.868713};
    rvec refForce04_{0.498928, -0.930625, 0.073129};
    rvec refForce42_{-0.239987, 1.22817, 1.31489};
    rvec refForce31_{-1.68605, 1.88135, 1.09365};
    // Bonds
    rvec refBond12_{ 1.9578, -0.853636, -0.477851 };
    rvec refBond23_{ -0.590743, 2.42451, 1.43378 };
    rvec refBond34_{ 0.0916361, -0.856599, -1.66937 };
    // Angles
    rvec refAngle123_[3]{{7.03468, 12.0566, 7.28362},
            {-22.5764, -14.7529, -9.12773},
            {15.5417, 2.69626, 1.84411}
    };
    rvec refAngle234_[3]{{1.07256, -2.1714, 4.11372},
            {-2.49495, 6.15754, -6.23719},
            {1.42239, -3.98614, 2.12348}
    };
    //Dihedrals
    rvec refDihedral1234_[4]{
            {-2.6624, -2.38199, -6.65288},
            {0.835379, 5.83056, 0.068634},
            {11.4507, -11.5021, 11.245},
            {-9.6237, 8.05354, -4.66076}
    };
    auto frame = create_reference("withDihedral_closeBy.gro", "closeBy.tpr", 
                                  "full_poly_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.add_2body_nocheck(0, 3, vda::InteractionType_LJ, refForce03_, 1);
    vda_instance.add_2body_nocheck(0, 4, vda::InteractionType_LJ, refForce04_, 1);
    vda_instance.add_2body_nocheck(3, 1, vda::InteractionType_LJ, refForce31_, 1);
    vda_instance.add_2body_nocheck(4, 2, vda::InteractionType_LJ, refForce42_, 1);
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refBond12_, 0);
    vda_instance.add_2body_nocheck(2, 3, vda::InteractionType_BOND, refBond23_, 0);
    vda_instance.add_2body_nocheck(3, 4, vda::InteractionType_BOND, refBond34_, 0);
    vda_instance.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance.add_angle(2, 3, 4, refAngle234_[0], refAngle234_[1], refAngle234_[2]);
    vda_instance.add_dihedral(1, 2, 3, 4, refDihedral1234_[0], refDihedral1234_[1],
                             refDihedral1234_[2], refDihedral1234_[3]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -5437.9204);
    EXPECT_NEAR(vda_instance.get_dU(), 55.29, 1e-2);
}

TEST_F(VDATest, Virial_ImproperDihedral)
{
    // !! they calculate half of our virial !!
    //Dihedrals
    rvec refDihedral1234_[4]{
            {-8.45504e+01, -9.55945e+01,  1.76142e+02},
            { 3.15944e+02,  2.78891e+02, -4.08395e+02},
            {-5.34421e+02, -3.94989e+02,  4.45987e+02},
            { 3.03028e+02,  2.11692e+02, -2.13735e+02}
    };
    auto frame = create_reference("justImp_bigBox.gro", "justImproper-NVE-vv_bigBox.tpr",
                                  "default_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_dihedral(0, 1, 2, 3, refDihedral1234_[0], refDihedral1234_[1],
                             refDihedral1234_[2], refDihedral1234_[3]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -555.70941);
    EXPECT_NEAR(vda_instance.get_dU(), 0, 3e-3);
}

TEST_F(VDATest, Virial_Dihedral)
{
    // !! they calculate half of our virial !!
    //LJ
    rvec refForce01_{-0.263863, 0.00894439, 1.82692};
    rvec refForce02_{0.887961, -0.525766, 2.08554};
    rvec refForce03_{0.722422, -1.03668, 0.868713};
    rvec refForce04_{0.498928, -0.930625, 0.073129};
    rvec refForce42_{-0.239987, 1.22817, 1.31489};
    rvec refForce31_{-1.68605, 1.88135, 1.09365};
    // Bonds
    rvec refBond12_{ 1.9578, -0.853636, -0.477851 };
    rvec refBond23_{ -0.590743, 2.42451, 1.43378 };
    rvec refBond34_{ 0.0916361, -0.856599, -1.66937 };
    // Angles
    rvec refAngle123_[3]{{7.03468, 12.0566, 7.28362},
            {-22.5764, -14.7529, -9.12773},
            {15.5417, 2.69626, 1.84411}
    };
    rvec refAngle234_[3]{{1.07256, -2.1714, 4.11372},
            {-2.49495, 6.15754, -6.23719},
            {1.42239, -3.98614, 2.12348}
    };
    //Dihedrals
    rvec refDihedral1234_[4]{
            {-2.6624, -2.38199, -6.65288},
            {0.835379, 5.83056, 0.068634},
            {11.4507, -11.5021, 11.245},
            {-9.6237, 8.05354, -4.66076}
    };
    auto frame = create_reference("withDihedral_closeBy.gro", "closeBy.tpr", 
                                  "default_box.vdi");
    auto refX_ = frame.refX_;
    auto refV_ = frame.refV_;
    VDA   vda_instance(refX_, refV_, frame.refBox_, frame.masses_.data(), frame.vda_settings_, wcycle);
    vda_instance.calc_shifts();
    vda_instance.add_2body_nocheck(0, 1, vda::InteractionType_LJ, refForce01_, 1);
    vda_instance.add_2body_nocheck(0, 2, vda::InteractionType_LJ, refForce02_, 1);
    vda_instance.add_2body_nocheck(0, 3, vda::InteractionType_LJ, refForce03_, 1);
    vda_instance.add_2body_nocheck(0, 4, vda::InteractionType_LJ, refForce04_, 1);
    vda_instance.add_2body_nocheck(3, 1, vda::InteractionType_LJ, refForce31_, 1);
    vda_instance.add_2body_nocheck(4, 2, vda::InteractionType_LJ, refForce42_, 1);
    vda_instance.add_2body_nocheck(1, 2, vda::InteractionType_BOND, refBond12_, 0);
    vda_instance.add_2body_nocheck(2, 3, vda::InteractionType_BOND, refBond23_, 0);
    vda_instance.add_2body_nocheck(3, 4, vda::InteractionType_BOND, refBond34_, 0);
    vda_instance.add_angle(1, 2, 3, refAngle123_[0], refAngle123_[1], refAngle123_[2]);
    vda_instance.add_angle(2, 3, 4, refAngle234_[0], refAngle234_[1], refAngle234_[2]);
    vda_instance.add_dihedral(1, 2, 3, 4, refDihedral1234_[0], refDihedral1234_[1],
                             refDihedral1234_[2], refDihedral1234_[3]);
    vda_instance.calc_dT();
    vda_instance.calc_dU();
    
    EXPECT_FLOAT_EQ(vda_instance.get_dT(), -1414.9152);
    EXPECT_NEAR(vda_instance.get_dU(), 3.3878, 1e-4);
}

TEST_F(VDATest, TestThrows)
{
    EXPECT_THROW(
            create_reference("decaneWater.gro", "decaneWater.tpr", "invalid_deformation_box.vdi");,
            gmx::InvalidInputError
            );
    EXPECT_THROW(
            create_reference("decaneWater.gro", "decaneWater.tpr", "invalid_domain_box.vdi");,
            gmx::InvalidInputError
            );
}
}  // namespace vda