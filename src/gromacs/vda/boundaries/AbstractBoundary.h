//
// Created by christ on 14.09.20.
//

#ifndef GROMACS_ABSTRACTBOUNDARY_H
#define GROMACS_ABSTRACTBOUNDARY_H
#include <utility>
#include <iostream>
#include <vector>
#include "gromacs/utility/real.h"
#include "gromacs/math/vectypes.h"
namespace vda {
class AbstractBoundary
{
public:
    virtual ~AbstractBoundary() = default;
    virtual bool                  contains(const gmx::RVec& vec) const = 0;
    virtual bool                  contains(const gmx::RVec&, const gmx::RVec&) const = 0;
    virtual std::vector<int>      contains_any_image(const gmx::RVec& vec) = 0;
    virtual std::pair<real, real> get_x_bounds() const = 0;
    virtual std::pair<real, real> get_y_bounds() const = 0;
    virtual std::pair<real, real> get_z_bounds() const = 0;
    virtual real get_volume() const = 0;
    
    virtual void set_bounds( real, real, real, real, real, real ) = 0;
    virtual void set_lengths( real, real, real) = 0;
    int count{0}; // FIXME: remove me, I am only here for debugging, don't forget to make
                           // `contains_any_image` const
    friend std::ostream& operator<<(std::ostream& os, const AbstractBoundary& boundary)
    {
        auto xbounds = boundary.get_x_bounds();
        auto ybounds = boundary.get_y_bounds();
        auto zbounds = boundary.get_z_bounds();
        os << "Boundaries:\n";
        os << "\tx: (" <<xbounds.first <<", " <<xbounds.second <<")\n";
        os << "\ty: (" <<ybounds.first <<", " <<ybounds.second <<")\n";
        os << "\tz: (" <<zbounds.first <<", " <<zbounds.second <<")";
        return os;
    }
};
}


#endif // GROMACS_ABSTRACTBOUNDARY_H
