//
// Created by christ on 14.09.20.
//

#ifndef GROMACS_POLYNOMIALDEFORMATION_H
#define GROMACS_POLYNOMIALDEFORMATION_H
#include <xtensor/xfixed.hpp>
#include "gromacs/utility/real.h"
#include "gromacs/math/vectypes.h"
#include "gromacs/gpu_utils/hostallocator.h"
#include "AbstractDeformation.h"

namespace vda
{
class PolynomialDeformation : public AbstractDeformation
{
public:
    PolynomialDeformation(const std::string& filename);

    xt::xtensor_fixed<real, xt::xshape<3>> derivative(rvec const& x) const override;
    xt::xtensor_fixed<real, xt::xshape<3,3>> jacobian(rvec const& x) const override;
    
    std::string get_name() const override {return name_;}
private:
    xt::xtensor_fixed<real, xt::xshape<3>> _a0;
    xt::xtensor_fixed<real, xt::xshape<3, 3>> _a1;
    xt::xtensor_fixed<real, xt::xshape<3, 3, 3>> _a2;
    xt::xtensor_fixed<real, xt::xshape<3, 3, 3, 3>> _a3;
    
    std::string name_ {"polynomial"};
};
}


#endif // GROMACS_POLYNOMIALDEFORMATION_H
