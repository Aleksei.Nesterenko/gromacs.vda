//
// Created by christ on 10.09.20.
//

#ifndef GROMACS_VDA_H
#define GROMACS_VDA_H

#include <unordered_map>
#include "gromacs/vda/VDASettings.h"
#include "gromacs/mdlib/mdatoms.h"
#include "gromacs/mdlib/constr.h"
#include "gromacs/timing/wallcycle.h"
#include "gromacs/vda/boundaries/AbstractBoundary.h"
#include "gromacs/vda/deformations/AbstractDeformation.h"
#include "ParticipatingInteraction.h"

#ifdef __cplusplus
/**
 * \brief Force analysis class. This deviates from the VDA class in that it does no decomposition
 * of the forces to pairwise forces. Instead it records the forces per atom and interaction
 * type.
 */
 // TODO: decouple from FDA
class VDA
{
public:
    /// \brief Auxiliary selector type for set_xp function
    enum vdaVecChoice 
    {
        vdaVecCoord, 
        vdaVecVel, 
        vdaVecCoordC, 
        vdaVecVelC,
        vdaVecForce
    };
    /// Default constructor
    VDA(int natoms, const matrix& box, vda::VDASettings& vda_settings, gmx_wallcycle_t wc);
    /// For backward compatibility with all tests
    /// TODO: remove it
    VDA(const gmx::PaddedHostVector<gmx::RVec> &x,
        const gmx::PaddedHostVector<gmx::RVec> &v,
        const matrix &box, real *m, vda::VDASettings &vda_settings, gmx_wallcycle_t wc);
    /// Copy constructor (wasn't tested!)
    VDA(VDA const& vda);
    ~VDA();
    
    /**
     * Add a particular type of nonbonded interaction for the kernels where only one type of interaction is computed;
     * force is passed as scalar along with the distance vector (as dx, dy, dz) from which the vector force is
     * computed, the same way it's done in the nonbonded kernels
     */
    void add_nonbonded(int i, int j, vda::InteractionType type, real force, 
                        real dx, real dy, real dz);
    void add_posre(int i, vda::InteractionType type, rvec force);
    void add_bond(int i, int j, vda::InteractionType type, rvec force);
    void add_dihedral(int i, int j, int k, int l, real* f_i, real* f_j, real* f_k, real* f_l);
    void add_angle(int ai, int aj, int ak, real* f_i, real* f_j, real* f_k);
    
    /// auxiliary function
    void add_2body_nocheck(int i, int j, vda::InteractionType type, rvec force, int nthread);

    void save_and_write_scalar_time_averages();

    void set_masses( const real* masses ) { masses_ = masses; }
    void set_lengths( real Lx, real Ly, real Lz ) { boundary_->set_lengths(Lx, Ly, Lz); }
    vda::VDASettings& get_settings() const {return vda_settings_;}
    double get_dT() const {return dT_;}
    double get_dU() const {return dU_;}

    /// @brief Public function requesting calculation of constraint forces
    /// @param dt time step
    /// @param integrator integrator type (eiVV, eiMD, etc) 
    void calc_constr_forces(real dt, int integrator);

    /* something about pbc */
    void calc_shifts();
    /* Compute change of the kinetic energy, T.
       It uses coordinates (x_), velocities (v_), shift vectors, and deformation Jacobian.
       - sum over n (1-N), i,j (x,y,z) of m_n*v_n(i)*v_n(j)*J_ij(x_n)
     */
    void calc_dT();
    void calc_dU();
    void clear();

    template<class T>
    void set_xp(const PaddedVector<gmx::RVec, gmx::Allocator<gmx::RVec, T> > *src, vdaVecChoice ch); 

    void _dump_var(vdaVecChoice ch);

private:
    void _calc_dU_routine0(int pp);

    void write_frame(std::ofstream& os) const;
    /// TODO: fill constraint clusters just from the topology
    void fill_constraint_clusters();

    /// @brief Private method for calculating constraint force
    /// @param dt - step size
    /// @param typ - 00
    //               |└ use x-constraint force (1) or v-constraint (0) 
    //               └ use MD (1) or VV(0)
    // 
    void _calc_cforces(real dt, int typ);

    int                                     nsteps_{ 0 };
    gmx::PaddedHostVector<gmx::RVec>        x_,
                                            v_,
                                            f_,
    /// Position one timestep ahead. Only needed for constraints.
                                            xPrime_,
                                            vPrime_,
                                            fConstr_;
    std::vector<std::vector<int>>           shift_vectors_;
    const matrix&                           box_;
    const real*                             masses_;

    double dT_;
    double dU_;

    double dU1_; /// contribution of constraints (pure locally)
    double dU2_; /// contribution of constraints (pure distributed)

    int boThreads, nbThreads; /// number of openMP threads for bonded and nonbonded interactions
    std::vector<std::vector<vda::ParticipatingInteraction> > participants_;
    std::shared_ptr<vda::AbstractBoundary>     boundary_;
    std::shared_ptr<vda::AbstractDeformation>  deformation_;
    std::vector<std::vector<int>> clusters_;
    
    vda::VDASettings& vda_settings_;
    /// Result file
    std::ofstream result_file_;
    gmx_wallcycle_t wcycle;
};

#else

/// Dummy class for nonbonded kernels
struct VDA {};

#endif

#ifdef __cplusplus
extern "C" {
#endif

void vda_add_nonbonded(class VDA *vda, int i, int j, real pf_coul, real pf_lj, real dx, real dy, real dz);

/**
 * Add coulomb part of nonbonded interaction
 */
void vda_add_nonbonded_coulomb(class VDA *vda, int i, int j, real force, real dx, real dy, real dz);

/**
 * Add lennard-jones part of nonbonded interaction
 */
void vda_add_nonbonded_lj(class VDA *vda, int i, int j, real force, real dx, real dy, real dz);

/**
 * Origin on j, but for 2 atoms it doesn't matter.
 */
void vda_virial_bond(class VDA *vda, int ai, int aj, real fbond, real dx, real dy, real dz);

#ifdef __cplusplus
}
#endif
#endif // GROMACS_VDA_H
