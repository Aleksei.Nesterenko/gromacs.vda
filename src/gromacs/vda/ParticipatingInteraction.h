//
// Created by christ on 19.11.20.
//

#ifndef GROMACS_PARTICIPATINGINTERACTION_H
#define GROMACS_PARTICIPATINGINTERACTION_H
#include <vector>
#include <array>
#include "gmxpre.h"
#include "gromacs/utility/real.h"
#include "gromacs/vda/PureInteractionType.h"
#include "gromacs/vda/Vector.h"
#include "gromacs/math/vectypes.h"

namespace vda
{
class ParticipatingInteraction
{
public:
    ParticipatingInteraction(real weight, vda::PureInteractionType type,
                             vda::Vector *forces, int *members);
    /// 1-body interaction constructor
    ParticipatingInteraction(real weight, vda::PureInteractionType type,
                             int i, const vda::Vector &fi);
    /// 2-body interaction constructor
    ParticipatingInteraction(real weight, vda::PureInteractionType type,
                             int i, int j, const vda::Vector &fi);
    /// 3-body interaction constructor
    ParticipatingInteraction(real weight, vda::PureInteractionType type,
                             int i, int j, int k, const vda::Vector &fi, const vda::Vector &fj, 
                             const vda::Vector &fk);
    /// 4-body interaction constructor
    ParticipatingInteraction(real weight, vda::PureInteractionType type,
                             int i, int j, int k, int l, const vda::Vector &fi, const vda::Vector &fj, 
                             const vda::Vector &fk, const vda::Vector &fl);
    ParticipatingInteraction( ParticipatingInteraction const& pi);
    
    real get_weight() const {return weight_;}
    constexpr short unsigned get_num() { return get_nbody(interactionType); }
    const int *get_members() const { return members_; }
    const vda::Vector *get_forces() const { return forces_; }
    friend std::ostream& operator << (std::ostream& os, const ParticipatingInteraction& PI)
    {
        os << vda::to_string(vda::from_pure(PI.interactionType)) << ":\t";
        std::string number;
        for( int i=0; i<get_nbody(PI.interactionType); ++i )
        {
            number += std::to_string(PI.members_[i]);
        }
        for( int i=0; i<get_nbody(PI.interactionType); ++i )
        {
            if( i != 0 ) { os <<"\t"; }
            os <<"(" <<number;
            os <<") -> " <<std::to_string(PI.members_[i]) <<": " << PI.forces_[i] <<std::endl;
        }
        return os;
    }
private:
    vda::PureInteractionType interactionType;
    real                     weight_;
    int                      members_[4] {-1,-1,-1,-1};
    vda::Vector              forces_[4]; // 0 0 0 0 by constructor 
};
}


#endif // GROMACS_PARTICIPATINGINTERACTION_H
